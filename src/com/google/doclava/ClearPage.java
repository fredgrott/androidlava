/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.JSilver;
import com.google.clearsilver.jsilver.data.Data;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class ClearPage.
 */
public class ClearPage {
  /*
   * public ClearPage() { String templ = "templates/index.cs"; String filename = "docs/index.html";
   * 
   * data.setValue("A.B.C", "1"); data.setValue("A.B.D", "2"); }
   */

  /** The m template dirs. */
  private static ArrayList<String> mTemplateDirs = new ArrayList<String>();
  
  /** The m template dir set. */
  private static boolean mTemplateDirSet = false;

  /** The m bundled template dirs. */
  private static ArrayList<String> mBundledTemplateDirs = new ArrayList<String>();

  /** The output dir. */
  public static String outputDir = "docs";
  
  /** The html dirs. */
  public static List<String> htmlDirs = new ArrayList<String>();
  
  /** The toroot. */
  public static String toroot = null;

  /**
     * Adds the template dir.
     * 
     * @param dir the dir
     */
  public static void addTemplateDir(String dir) {
    mTemplateDirSet = true;
    mTemplateDirs.add(dir);
  }

  /**
     * Gets the template dirs.
     * 
     * @return the template dirs
     */
  public static List<String> getTemplateDirs() {
    return mTemplateDirs;
  }

  /**
     * Adds the bundled template dir.
     * 
     * @param dir the dir
     */
  public static void addBundledTemplateDir(String dir) {
    mTemplateDirSet = true;
    mBundledTemplateDirs.add(dir);
  }

  /**
     * Gets the bundled template dirs.
     * 
     * @return the bundled template dirs
     */
  public static List<String> getBundledTemplateDirs() {
    return mBundledTemplateDirs;
  }

  /**
     * Count slashes.
     * 
     * @param s the s
     * @return the int
     */
  private static int countSlashes(String s) {
    final int N = s.length();
    int slashcount = 0;
    for (int i = 0; i < N; i++) {
      if (s.charAt(i) == '/') {
        slashcount++;
      }
    }
    return slashcount;
  }

  /**
     * Write.
     * 
     * @param data the data
     * @param templ the templ
     * @param filename the filename
     * @param cs the cs
     */
  public static void write(Data data, String templ, String filename, JSilver cs) {
    write(data, templ, filename, false, cs);
  }

  /**
     * Write.
     * 
     * @param data the data
     * @param templ the templ
     * @param filename the filename
     */
  public static void write(Data data, String templ, String filename) {
    write(data, templ, filename, false, Doclava.jSilver);
  }

  /**
     * Write.
     * 
     * @param data the data
     * @param templ the templ
     * @param filename the filename
     * @param fullPath the full path
     */
  public static void write(Data data, String templ, String filename, boolean fullPath) {
    write(data, templ, filename, false, Doclava.jSilver);
  }

  /**
     * Write.
     * 
     * @param data the data
     * @param templ the templ
     * @param filename the filename
     * @param fullPath the full path
     * @param cs the cs
     */
  public static void write(Data data, String templ, String filename, boolean fullPath, JSilver cs) {
    if (!htmlDirs.isEmpty()) {
      data.setValue("hasindex", "true");
    }

    String toroot;
    if (ClearPage.toroot != null) {
      toroot = ClearPage.toroot;
    } else {
      int slashcount = countSlashes(filename);
      if (slashcount > 0) {
        toroot = "";
        for (int i = 0; i < slashcount; i++) {
          toroot += "../";
        }
      } else {
        toroot = "./";
      }
    }
    data.setValue("toroot", toroot);

    data.setValue("filename", filename);

    if (!fullPath) {
      filename = outputDir + "/" + filename;
    }

    int i = 0;
    if (!htmlDirs.isEmpty()) {
        for (String dir : htmlDirs) {
          data.setValue("hdf.loadpaths." + i, dir);
          i++;
        }
    }
    if (mTemplateDirSet) {
      for (String dir : mTemplateDirs) {
        data.setValue("hdf.loadpaths." + i, dir);
        i++;
      }
    } else {
      data.setValue("hdf.loadpaths." + i, "templates");
    }

    File file = new File(outputFilename(filename));

    ensureDirectory(file);

    Writer stream = null;
    try {
      stream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
      String rendered = cs.render(templ, data);
      stream.write(rendered, 0, rendered.length());
    } catch (IOException e) {
      System.out.println("error: " + e.getMessage() + "; when writing file: " + filename);
    } finally {
      if (stream != null) {
        try {
          stream.close();
        } catch (IOException e) {}
      }
    }
  }

  // recursively create the directories to the output
  /**
     * Ensure directory.
     * 
     * @param f the f
     */
  public static void ensureDirectory(File f) {
    File parent = f.getParentFile();
    if (parent != null) {
      parent.mkdirs();
    }
  }

  /**
     * Copy file.
     * 
     * @param from the from
     * @param toPath the to path
     */
  @SuppressWarnings("resource")
public static void copyFile(File from, String toPath) {
    File to = new File(outputDir + "/" + toPath);
    FileInputStream in;
    FileOutputStream out;
    try {
      if (!from.exists()) {
        throw new IOException();
      }
      in = new FileInputStream(from);
    } catch (IOException e) {
      System.err.println(from.getAbsolutePath() + ": Error opening file");
      return;
    }
    ensureDirectory(to);
    try {
      out = new FileOutputStream(to);
    } catch (IOException e) {
      System.err.println(from.getAbsolutePath() + ": Error opening file");
      return;
    }

    long sizel = from.length();
    final int maxsize = 64 * 1024;
    int size = sizel > maxsize ? maxsize : (int) sizel;
    byte[] buf = new byte[size];
    while (true) {
      try {
        size = in.read(buf);
      } catch (IOException e) {
        System.err.println(from.getAbsolutePath() + ": error reading file");
        break;
      }
      if (size > 0) {
        try {
          out.write(buf, 0, size);
        } catch (IOException e) {
          System.err.println(from.getAbsolutePath() + ": error writing file");
        }
      } else {
        break;
      }
    }
    try {
      in.close();
    } catch (IOException e) {}
    try {
      out.close();
    } catch (IOException e) {}
  }

  /**
     * Takes a string that ends w/ .html and changes the .html to htmlExtension
     * 
     * @param htmlFile the html file
     * @return the string
     */
  public static String outputFilename(String htmlFile) {
    if (!Doclava.htmlExtension.equals(".html") && htmlFile.endsWith(".html")) {
      return htmlFile.substring(0, htmlFile.length() - 5) + Doclava.htmlExtension;
    } else {
      return htmlFile;
    }
  }

}
