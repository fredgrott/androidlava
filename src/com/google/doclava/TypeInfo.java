/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;

import java.util.*;

// TODO: Auto-generated Javadoc
/**
 * The Class TypeInfo.
 */
public class TypeInfo implements Resolvable {
  
  /** The Constant PRIMITIVE_TYPES. */
  public static final Set<String> PRIMITIVE_TYPES = Collections.unmodifiableSet(
      new HashSet<String>(Arrays.asList("boolean", "byte", "char", "double", "float", "int",
      "long", "short", "void")));
  
  /**
     * Instantiates a new type info.
     * 
     * @param isPrimitive the is primitive
     * @param dimension the dimension
     * @param simpleTypeName the simple type name
     * @param qualifiedTypeName the qualified type name
     * @param cl the cl
     */
  public TypeInfo(boolean isPrimitive, String dimension, String simpleTypeName,
      String qualifiedTypeName, ClassInfo cl) {
    mIsPrimitive = isPrimitive;
    mDimension = dimension;
    mSimpleTypeName = simpleTypeName;
    mQualifiedTypeName = qualifiedTypeName;
    mClass = cl;
  }

  /**
     * Instantiates a new type info.
     * 
     * @param typeString the type string
     */
  public TypeInfo(String typeString) {
    // VarArgs
    if (typeString.endsWith("...")) {
      typeString = typeString.substring(0, typeString.length() - 3);
    }
    
    // Generic parameters
    int paramStartPos = typeString.indexOf('<');
    if (paramStartPos > -1) {
      ArrayList<TypeInfo> generics = new ArrayList<TypeInfo>();
      int paramEndPos = typeString.lastIndexOf('>');
      
      int entryStartPos = paramStartPos + 1;
      int bracketNesting = 0;
      for (int i = entryStartPos; i < paramEndPos; i++) {
        char c = typeString.charAt(i);
        if (c == ',' && bracketNesting == 0) {
          String entry = typeString.substring(entryStartPos, i).trim();
          TypeInfo info = new TypeInfo(entry);
          generics.add(info);
          entryStartPos = i + 1;
        } else if (c == '<') {
          bracketNesting++;
        } else if (c == '>') {
          bracketNesting--;
        }
      }
     
      TypeInfo info = new TypeInfo(typeString.substring(entryStartPos, paramEndPos).trim());
      generics.add(info);
      
      mTypeArguments = generics;
      
      if (paramEndPos < typeString.length() - 1) {
        typeString = typeString.substring(0,paramStartPos) + typeString.substring(paramEndPos + 1);
      } else {
        typeString = typeString.substring(0,paramStartPos);
      }
    }
    
    // Dimensions
    int pos = typeString.indexOf('['); 
    if (pos > -1) {
      mDimension = typeString.substring(pos);
      typeString = typeString.substring(0, pos);
    } else {
      mDimension = "";
    }
   
    if (PRIMITIVE_TYPES.contains(typeString)) {
      mIsPrimitive = true;
      mSimpleTypeName = typeString;
      mQualifiedTypeName = typeString;
    } else {
      mQualifiedTypeName = typeString;
      pos = typeString.lastIndexOf('.');
      if (pos > -1) {
        mSimpleTypeName = typeString.substring(pos + 1);
      } else {
        mSimpleTypeName = typeString;
      }
    }
  }

  /**
     * As class info.
     * 
     * @return the class info
     */
  public ClassInfo asClassInfo() {
    return mClass;
  }

  /**
     * Checks if is primitive.
     * 
     * @return true, if is primitive
     */
  public boolean isPrimitive() {
    return mIsPrimitive;
  }

  /**
     * Dimension.
     * 
     * @return the string
     */
  public String dimension() {
    return mDimension;
  }

  /**
     * Sets the dimension.
     * 
     * @param dimension the new dimension
     */
  public void setDimension(String dimension) {
      mDimension = dimension;
  }

  /**
     * Simple type name.
     * 
     * @return the string
     */
  public String simpleTypeName() {
    return mSimpleTypeName;
  }

  /**
     * Qualified type name.
     * 
     * @return the string
     */
  public String qualifiedTypeName() {
    return mQualifiedTypeName;
  }

  /**
     * Full name.
     * 
     * @return the string
     */
  public String fullName() {
    if (mFullName != null) {
      return mFullName;
    } else {
      return fullName(new HashSet<String>());
    }
  }

  /**
     * Type arguments name.
     * 
     * @param args the args
     * @param typeVars the type vars
     * @return the string
     */
  public static String typeArgumentsName(ArrayList<TypeInfo> args, HashSet<String> typeVars) {
    String result = "<";

    int i = 0;
    for (TypeInfo arg : args) {
      result += arg.fullName(typeVars);
      if (i != (args.size()-1)) {
        result += ", ";
      }
      i++;
    }
    result += ">";
    return result;
  }

  /**
     * Full name.
     * 
     * @param typeVars the type vars
     * @return the string
     */
  public String fullName(HashSet<String> typeVars) {
    mFullName = fullNameNoDimension(typeVars) + mDimension;
    return mFullName;
  }

  /**
     * Full name no dimension.
     * 
     * @param typeVars the type vars
     * @return the string
     */
  public String fullNameNoDimension(HashSet<String> typeVars) {
    String fullName = null;
    if (mIsTypeVariable) {
      if (typeVars.contains(mQualifiedTypeName)) {
        // don't recurse forever with the parameters. This handles
        // Enum<K extends Enum<K>>
        return mQualifiedTypeName;
      }
      typeVars.add(mQualifiedTypeName);
    }
    /*
     * if (fullName != null) { return fullName; }
     */
    fullName = mQualifiedTypeName;
    if (mTypeArguments != null && !mTypeArguments.isEmpty()) {
      fullName += typeArgumentsName(mTypeArguments, typeVars);
    } else if (mSuperBounds != null && !mSuperBounds.isEmpty()) {
        for (TypeInfo superBound : mSuperBounds) {
            if (superBound == mSuperBounds.get(0)) {
                fullName += " super " + superBound.fullName(typeVars);
            } else {
                fullName += " & " + superBound.fullName(typeVars);
            }
        }
    } else if (mExtendsBounds != null && !mExtendsBounds.isEmpty()) {
        for (TypeInfo extendsBound : mExtendsBounds) {
            if (extendsBound == mExtendsBounds.get(0)) {
                fullName += " extends " + extendsBound.fullName(typeVars);
            } else {
                fullName += " & " + extendsBound.fullName(typeVars);
            }
        }
    }
    return fullName;
  }

  /**
     * Type arguments.
     * 
     * @return the array list
     */
  public ArrayList<TypeInfo> typeArguments() {
    return mTypeArguments;
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     */
  public void makeHDF(Data data, String base) {
    makeHDFRecursive(data, base, false, false, new HashSet<String>());
  }

  /**
     * Make qualified hdf.
     * 
     * @param data the data
     * @param base the base
     */
  public void makeQualifiedHDF(Data data, String base) {
    makeHDFRecursive(data, base, true, false, new HashSet<String>());
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param isLastVararg the is last vararg
     * @param typeVariables the type variables
     */
  public void makeHDF(Data data, String base, boolean isLastVararg, HashSet<String> typeVariables) {
    makeHDFRecursive(data, base, false, isLastVararg, typeVariables);
  }

  /**
     * Make qualified hdf.
     * 
     * @param data the data
     * @param base the base
     * @param typeVariables the type variables
     */
  public void makeQualifiedHDF(Data data, String base, HashSet<String> typeVariables) {
    makeHDFRecursive(data, base, true, false, typeVariables);
  }

  /**
     * Make hdf recursive.
     * 
     * @param data the data
     * @param base the base
     * @param qualified the qualified
     * @param isLastVararg the is last vararg
     * @param typeVars the type vars
     */
  private void makeHDFRecursive(Data data, String base, boolean qualified, boolean isLastVararg,
      HashSet<String> typeVars) {
    String label = qualified ? qualifiedTypeName() : simpleTypeName();
    label += (isLastVararg) ? "..." : dimension();
    data.setValue(base + ".label", label);
    if (mIsTypeVariable || mIsWildcard) {
      // could link to an @param tag on the class to describe this
      // but for now, just don't make it a link
    } else if (!isPrimitive() && mClass != null) {
      if (mClass.isIncluded()) {
        data.setValue(base + ".link", mClass.htmlPage());
        data.setValue(base + ".since", mClass.getSince());
      } else {
        Doclava.federationTagger.tag(mClass);
        if (!mClass.getFederatedReferences().isEmpty()) {
          FederatedSite site = mClass.getFederatedReferences().iterator().next();
          data.setValue(base + ".link", site.linkFor(mClass.htmlPage()));
          data.setValue(base + ".federated", site.name());
        }
      }
    }

    if (mIsTypeVariable) {
      if (typeVars.contains(qualifiedTypeName())) {
        // don't recurse forever with the parameters. This handles
        // Enum<K extends Enum<K>>
        return;
      }
      typeVars.add(qualifiedTypeName());
    }
    if (mTypeArguments != null) {
      TypeInfo.makeHDF(data, base + ".typeArguments", mTypeArguments, qualified, typeVars);
    }
    if (mSuperBounds != null) {
      TypeInfo.makeHDF(data, base + ".superBounds", mSuperBounds, qualified, typeVars);
    }
    if (mExtendsBounds != null) {
      TypeInfo.makeHDF(data, base + ".extendsBounds", mExtendsBounds, qualified, typeVars);
    }
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param types the types
     * @param qualified the qualified
     * @param typeVariables the type variables
     */
  public static void makeHDF(Data data, String base, ArrayList<TypeInfo> types, boolean qualified,
      HashSet<String> typeVariables) {
    int i = 0;
    for (TypeInfo type : types) {
      type.makeHDFRecursive(data, base + "." + i++, qualified, false, typeVariables);
    }
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param types the types
     * @param qualified the qualified
     */
  public static void makeHDF(Data data, String base, ArrayList<TypeInfo> types, boolean qualified) {
    makeHDF(data, base, types, qualified, new HashSet<String>());
  }

  /**
     * Sets the type arguments.
     * 
     * @param args the new type arguments
     */
  void setTypeArguments(ArrayList<TypeInfo> args) {
    mTypeArguments = args;
  }

  /**
     * Adds the type argument.
     * 
     * @param arg the arg
     */
  public void addTypeArgument(TypeInfo arg) {
      if (mTypeArguments == null) {
          mTypeArguments = new ArrayList<TypeInfo>();
      }

      mTypeArguments.add(arg);
  }

  /**
     * Sets the bounds.
     * 
     * @param superBounds the super bounds
     * @param extendsBounds the extends bounds
     */
  void setBounds(ArrayList<TypeInfo> superBounds, ArrayList<TypeInfo> extendsBounds) {
    mSuperBounds = superBounds;
    mExtendsBounds = extendsBounds;
  }

  /**
     * Super bounds.
     * 
     * @return the array list
     */
  public ArrayList<TypeInfo> superBounds() {
      return mSuperBounds;
  }

  /**
     * Extends bounds.
     * 
     * @return the array list
     */
  public ArrayList<TypeInfo> extendsBounds() {
      return mExtendsBounds;
  }

  /**
     * Sets the checks if is type variable.
     * 
     * @param b the new checks if is type variable
     */
  void setIsTypeVariable(boolean b) {
    mIsTypeVariable = b;
  }

  /**
     * Sets the checks if is wildcard.
     * 
     * @param b the new checks if is wildcard
     */
  void setIsWildcard(boolean b) {
    mIsWildcard = b;
  }

  /**
     * Checks if is wildcard.
     * 
     * @return true, if is wildcard
     */
  public boolean isWildcard() {
      return mIsWildcard;
  }

  /**
     * Type variables.
     * 
     * @param params the params
     * @return the hash set
     */
  static HashSet<String> typeVariables(ArrayList<TypeInfo> params) {
    return typeVariables(params, new HashSet<String>());
  }

  /**
     * Type variables.
     * 
     * @param params the params
     * @param result the result
     * @return the hash set
     */
  static HashSet<String> typeVariables(ArrayList<TypeInfo> params, HashSet<String> result) {
    if (params != null) {
        for (TypeInfo t : params) {
            if (t.mIsTypeVariable) {
                result.add(t.mQualifiedTypeName);
            }
        }
    }
    return result;
  }


  /**
     * Checks if is type variable.
     * 
     * @return true, if is type variable
     */
  public boolean isTypeVariable() {
    return mIsTypeVariable;
  }

  /**
     * Default value.
     * 
     * @return the string
     */
  public String defaultValue() {
    if (mIsPrimitive) {
      if ("boolean".equals(mSimpleTypeName)) {
        return "false";
      } else {
        return "0";
      }
    } else {
      return "null";
    }
  }

  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    String returnString = "";
    returnString +=
        "Primitive?: " + mIsPrimitive + " TypeVariable?: " + mIsTypeVariable + " Wildcard?: "
            + mIsWildcard + " Dimension: " + mDimension + " QualifedTypeName: "
            + mQualifiedTypeName;

    if (mTypeArguments != null) {
      returnString += "\nTypeArguments: ";
      for (TypeInfo tA : mTypeArguments) {
        returnString += tA.qualifiedTypeName() + "(" + tA + ") ";
      }
    }
    if (mSuperBounds != null) {
      returnString += "\nSuperBounds: ";
      for (TypeInfo tA : mSuperBounds) {
        returnString += tA.qualifiedTypeName() + "(" + tA + ") ";
      }
    }
    if (mExtendsBounds != null) {
      returnString += "\nExtendsBounds: ";
      for (TypeInfo tA : mExtendsBounds) {
        returnString += tA.qualifiedTypeName() + "(" + tA + ") ";
      }
    }
    return returnString;
  }

  /**
   * @see com.google.doclava.Resolvable#addResolution(com.google.doclava.Resolution)
   */
  public void addResolution(Resolution resolution) {
      if (mResolutions == null) {
          mResolutions = new ArrayList<Resolution>();
      }

      mResolutions.add(resolution);
  }

  /**
   * @see com.google.doclava.Resolvable#printResolutions()
   */
  public void printResolutions() {
      if (mResolutions == null || mResolutions.isEmpty()) {
          return;
      }

      System.out.println("Resolutions for Type " + mSimpleTypeName + ":");
      for (Resolution r : mResolutions) {
          System.out.println(r);
      }
  }

  /**
   * @see com.google.doclava.Resolvable#resolveResolutions()
   */
  public boolean resolveResolutions() {
      ArrayList<Resolution> resolutions = mResolutions;
      mResolutions = new ArrayList<Resolution>();

      boolean allResolved = true;
      for (Resolution resolution : resolutions) {
          if ("class".equals(resolution.getVariable())) {
              StringBuilder qualifiedClassName = new StringBuilder();
              InfoBuilder.resolveQualifiedName(resolution.getValue(), qualifiedClassName,
                      resolution.getInfoBuilder());

              // if we still couldn't resolve it, save it for the next pass
              if ("".equals(qualifiedClassName.toString())) {
                  mResolutions.add(resolution);
                  allResolved = false;
              } else {
                  mClass = InfoBuilder.Caches.obtainClass(qualifiedClassName.toString());
              }
          }
      }

      return allResolved;
  }

  /** The m resolutions. */
  private ArrayList<Resolution> mResolutions;

  /** The m is primitive. */
  private boolean mIsPrimitive;
  
  /** The m is type variable. */
  private boolean mIsTypeVariable;
  
  /** The m is wildcard. */
  private boolean mIsWildcard;
  
  /** The m dimension. */
  private String mDimension;
  
  /** The m simple type name. */
  private String mSimpleTypeName;
  
  /** The m qualified type name. */
  private String mQualifiedTypeName;
  
  /** The m class. */
  private ClassInfo mClass;
  
  /** The m type arguments. */
  private ArrayList<TypeInfo> mTypeArguments;
  
  /** The m super bounds. */
  private ArrayList<TypeInfo> mSuperBounds;
  
  /** The m extends bounds. */
  private ArrayList<TypeInfo> mExtendsBounds;
  
  /** The m full name. */
  private String mFullName;
}
