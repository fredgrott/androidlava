/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class MemberInfo.
 */
@SuppressWarnings("rawtypes")
public abstract class MemberInfo extends DocInfo implements Comparable, Scoped {
  
  /**
     * Instantiates a new member info.
     * 
     * @param rawCommentText the raw comment text
     * @param name the name
     * @param signature the signature
     * @param containingClass the containing class
     * @param realContainingClass the real containing class
     * @param isPublic the is public
     * @param isProtected the is protected
     * @param isPackagePrivate the is package private
     * @param isPrivate the is private
     * @param isFinal the is final
     * @param isStatic the is static
     * @param isSynthetic the is synthetic
     * @param kind the kind
     * @param position the position
     * @param annotations the annotations
     */
  public MemberInfo(String rawCommentText, String name, String signature,
      ClassInfo containingClass, ClassInfo realContainingClass, boolean isPublic,
      boolean isProtected, boolean isPackagePrivate, boolean isPrivate, boolean isFinal,
      boolean isStatic, boolean isSynthetic, String kind, SourcePositionInfo position,
      ArrayList<AnnotationInstanceInfo> annotations) {
    super(rawCommentText, position);
    mName = name;
    mSignature = signature;
    mContainingClass = containingClass;
    mRealContainingClass = realContainingClass;
    mIsPublic = isPublic;
    mIsProtected = isProtected;
    mIsPackagePrivate = isPackagePrivate;
    mIsPrivate = isPrivate;
    mIsFinal = isFinal;
    mIsStatic = isStatic;
    mIsSynthetic = isSynthetic;
    mKind = kind;
    mAnnotations = annotations;
  }

  /**
     * Checks if is executable.
     * 
     * @return true, if is executable
     */
  public abstract boolean isExecutable();

  /**
     * Anchor.
     * 
     * @return the string
     */
  public String anchor() {
    if (mSignature != null) {
      return mName + mSignature;
    } else {
      return mName;
    }
  }

  /**
   * @see com.google.doclava.DocInfo#htmlPage()
   */
  public String htmlPage() {
    return mContainingClass.htmlPage() + "#" + anchor();
  }

  /**
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(Object that) {
    return this.htmlPage().compareTo(((MemberInfo) that).htmlPage());
  }

  /**
     * Name.
     * 
     * @return the string
     */
  public String name() {
    return mName;
  }

  /**
     * Signature.
     * 
     * @return the string
     */
  public String signature() {
    return mSignature;
  }

  /**
     * Real containing class.
     * 
     * @return the class info
     */
  public ClassInfo realContainingClass() {
    return mRealContainingClass;
  }

  /**
     * Containing class.
     * 
     * @return the class info
     */
  public ClassInfo containingClass() {
    return mContainingClass;
  }

  /**
   * @see com.google.doclava.Scoped#isPublic()
   */
  public boolean isPublic() {
    return mIsPublic;
  }

  /**
   * @see com.google.doclava.Scoped#isProtected()
   */
  public boolean isProtected() {
    return mIsProtected;
  }

  /**
   * @see com.google.doclava.Scoped#isPackagePrivate()
   */
  public boolean isPackagePrivate() {
    return mIsPackagePrivate;
  }

  /**
   * @see com.google.doclava.Scoped#isPrivate()
   */
  public boolean isPrivate() {
    return mIsPrivate;
  }
  
  /**
     * Scope.
     * 
     * @return the string
     */
  public String scope() {
    if (isPublic()) {
      return "public";
    } else if (isProtected()) {
      return "protected";
    } else if (isPackagePrivate()) {
      return "";
    } else if (isPrivate()) {
      return "private";
    } else {
      throw new RuntimeException("invalid scope for object " + this);
    }
  }

  /**
     * Checks if is static.
     * 
     * @return true, if is static
     */
  public boolean isStatic() {
    return mIsStatic;
  }

  /**
     * Checks if is final.
     * 
     * @return true, if is final
     */
  public boolean isFinal() {
    return mIsFinal;
  }

  /**
     * Checks if is synthetic.
     * 
     * @return true, if is synthetic
     */
  public boolean isSynthetic() {
    return mIsSynthetic;
  }

  /**
   * @see com.google.doclava.DocInfo#parent()
   */
  @Override
  public ContainerInfo parent() {
    return mContainingClass;
  }

  /**
     * Check level.
     * 
     * @return true, if successful
     */
  public boolean checkLevel() {
    return Doclava.checkLevel(mIsPublic, mIsProtected, mIsPackagePrivate, mIsPrivate, isHidden());
  }

  /**
     * Kind.
     * 
     * @return the string
     */
  public String kind() {
    return mKind;
  }

  /**
     * Annotations.
     * 
     * @return the array list
     */
  public ArrayList<AnnotationInstanceInfo> annotations() {
    return mAnnotations;
  }

  /** The m containing class. */
  ClassInfo mContainingClass;
  
  /** The m real containing class. */
  ClassInfo mRealContainingClass;
  
  /** The m name. */
  String mName;
  
  /** The m signature. */
  String mSignature;
  
  /** The m is public. */
  boolean mIsPublic;
  
  /** The m is protected. */
  boolean mIsProtected;
  
  /** The m is package private. */
  boolean mIsPackagePrivate;
  
  /** The m is private. */
  boolean mIsPrivate;
  
  /** The m is final. */
  boolean mIsFinal;
  
  /** The m is static. */
  boolean mIsStatic;
  
  /** The m is synthetic. */
  boolean mIsSynthetic;
  
  /** The m kind. */
  String mKind;
  
  /** The m annotations. */
  private ArrayList<AnnotationInstanceInfo> mAnnotations;

}
