/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava.apicheck;

// TODO: Auto-generated Javadoc
/**
 * The Class ApiParseException.
 */
@SuppressWarnings("serial")
public final class ApiParseException extends Exception {
  
  /** The file. */
  public String file;
  
  /** The line. */
  public int line;

  /**
     * Instantiates a new api parse exception.
     */
  public ApiParseException() {
  }
  
  /**
     * Instantiates a new api parse exception.
     * 
     * @param message the message
     */
  public ApiParseException(String message) {
    super(message);
  }

  /**
     * Instantiates a new api parse exception.
     * 
     * @param message the message
     * @param cause the cause
     */
  public ApiParseException(String message, Exception cause) {
    super(message, cause);
    if (cause instanceof ApiParseException) {
        this.line = ((ApiParseException)cause).line;
    }
  }

  /**
     * Instantiates a new api parse exception.
     * 
     * @param message the message
     * @param line the line
     */
  public ApiParseException(String message, int line) {
    super(message);
    this.line = line;
  }
  
  /**
   * @see java.lang.Throwable#getMessage()
   */
  public String getMessage() {
    if (line > 0) {
      return super.getMessage() + " line " + line;
    } else {
      return super.getMessage();
    }
  }
}
