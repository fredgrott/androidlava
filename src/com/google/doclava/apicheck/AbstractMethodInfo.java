/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava.apicheck;

// TODO: Auto-generated Javadoc
/**
 * The Interface AbstractMethodInfo.
 */
public interface AbstractMethodInfo {

  /**
     * Adds the exception.
     * 
     * @param exec the exec
     */
  public void addException(String exec);

  /**
     * Adds the parameter.
     * 
     * @param p the p
     */
  public void addParameter(com.google.doclava.ParameterInfo p);

  /**
     * Sets the deprecated.
     * 
     * @param deprecated the new deprecated
     */
  public void setDeprecated(boolean deprecated);
  
  /**
     * Sets the varargs.
     * 
     * @param varargs the new varargs
     */
  public void setVarargs(boolean varargs);
  
  /**
     * Checks if is var args.
     * 
     * @return true, if is var args
     */
  public boolean isVarArgs();
}
