/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;

// TODO: Auto-generated Javadoc
/**
 * The Class TagInfo.
 */
public class TagInfo {
  
  /** The m name. */
  private String mName;
  
  /** The m text. */
  private String mText;
  
  /** The m kind. */
  private String mKind;
  
  /** The m position. */
  private SourcePositionInfo mPosition;

  /**
     * Instantiates a new tag info.
     * 
     * @param n the n
     * @param k the k
     * @param t the t
     * @param sp the sp
     */
  TagInfo(String n, String k, String t, SourcePositionInfo sp) {
    mName = n;
    mText = t;
    mKind = k;
    mPosition = sp;
  }

  /**
     * Name.
     * 
     * @return the string
     */
  String name() {
    return mName;
  }

  /**
     * Text.
     * 
     * @return the string
     */
  String text() {
    return mText;
  }

  /**
     * Kind.
     * 
     * @return the string
     */
  String kind() {
    return mKind;
  }

  /**
     * Position.
     * 
     * @return the source position info
     */
  SourcePositionInfo position() {
    return mPosition;
  }

  /**
     * Sets the kind.
     * 
     * @param kind the new kind
     */
  void setKind(String kind) {
    mKind = kind;
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     */
  public void makeHDF(Data data, String base) {
    data.setValue(base + ".name", name());
    data.setValue(base + ".text", text());
    data.setValue(base + ".kind", kind());
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param tags the tags
     */
  public static void makeHDF(Data data, String base, TagInfo[] tags) {
    makeHDF(data, base, tags, null, 0, 0);
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param tags the tags
     */
  public static void makeHDF(Data data, String base, InheritedTags tags) {
    makeHDF(data, base, tags.tags(), tags.inherited(), 0, 0);
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param tags the tags
     * @param inherited the inherited
     * @param j the j
     * @param depth the depth
     * @return the int
     */
  private static int makeHDF(Data data, String base, TagInfo[] tags, InheritedTags inherited,
      int j, int depth) {
    int i;
    int len = tags.length;
    if (len == 0 && inherited != null) {
      j = makeHDF(data, base, inherited.tags(), inherited.inherited(), j, depth + 1);
    } else {
      for (i = 0; i < len; i++, j++) {
        TagInfo t = tags[i];
        if (inherited != null && t.name().equals("@inheritDoc")) {
          j = makeHDF(data, base, inherited.tags(), inherited.inherited(), j, depth + 1);
        } else {
          if (t.name().equals("@inheritDoc")) {
            Errors.error(Errors.BAD_INHERITDOC, t.mPosition,
                "@inheritDoc on class/method that is not inherited");
          }
          t.makeHDF(data, base + "." + j);
        }
      }
    }
    return j;
  }
}
