/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import java.util.Set;
import java.util.TreeSet;

// TODO: Auto-generated Javadoc
/**
 * The Class Errors.
 */
public class Errors {
  
  /** The had error. */
  public static boolean hadError = false;
  
  /** The warnings are errors. */
  private static boolean warningsAreErrors = false;
  
  /** The all errors. */
  private static TreeSet<ErrorMessage> allErrors = new TreeSet<ErrorMessage>();

  /**
     * The Class ErrorMessage.
     */
  @SuppressWarnings("rawtypes")
public static class ErrorMessage implements Comparable {
    
    /** The error. */
    Error error;
    
    /** The pos. */
    SourcePositionInfo pos;
    
    /** The msg. */
    String msg;

    /**
     * Instantiates a new error message.
     * 
     * @param e the e
     * @param p the p
     * @param m the m
     */
    ErrorMessage(Error e, SourcePositionInfo p, String m) {
      error = e;
      pos = p;
      msg = m;
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Object o) {
      ErrorMessage that = (ErrorMessage) o;
      int r = this.pos.compareTo(that.pos);
      if (r != 0) return r;
      return this.msg.compareTo(that.msg);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
      String whereText = this.pos == null ? "unknown: " : this.pos.toString() + ':';
      return whereText + this.msg;
    }
    
    /**
     * Error.
     * 
     * @return the error
     */
    public Error error() {
      return error;
    }
  }

  /**
     * Error.
     * 
     * @param error the error
     * @param where the where
     * @param text the text
     */
  public static void error(Error error, SourcePositionInfo where, String text) {
    if (error.level == HIDDEN) {
      return;
    }

    int level = (!warningsAreErrors && error.level == WARNING) ? WARNING : ERROR;
    String which = level == WARNING ? " warning " : " error ";
    String message = which + error.code + ": " + text;

    if (where == null) {
      where = new SourcePositionInfo("unknown", 0, 0);
    }

    allErrors.add(new ErrorMessage(error, where, message));

    if (error.level == ERROR || (warningsAreErrors && error.level == WARNING)) {
      hadError = true;
    }
  }
  
  /**
     * Clear errors.
     */
  public static void clearErrors() {
    hadError = false;
    allErrors.clear();
  }

  /**
     * Prints the errors.
     */
  public static void printErrors() {
    printErrors(allErrors);
  }
  
  /**
     * Prints the errors.
     * 
     * @param errors the errors
     */
  public static void printErrors(Set<ErrorMessage> errors) {
    for (ErrorMessage m : errors) {
      if (m.error.level == WARNING) {
        System.err.println(m.toString());
      }
    }
    for (ErrorMessage m : errors) {
      if (m.error.level == ERROR) {
        System.err.println(m.toString());
      }
    }
  }
  
  /**
     * Gets the errors.
     * 
     * @return the errors
     */
  public static Set<ErrorMessage> getErrors() {
    return allErrors;
  }

  /** The hidden. */
  public static int HIDDEN = 0;
  
  /** The warning. */
  public static int WARNING = 1;
  
  /** The error. */
  public static int ERROR = 2;

  /**
     * Sets the warnings are errors.
     * 
     * @param val the new warnings are errors
     */
  public static void setWarningsAreErrors(boolean val) {
    warningsAreErrors = val;
  }

  /**
     * The Class Error.
     */
  public static class Error {
    
    /** The code. */
    public int code;
    
    /** The level. */
    public int level;

    /**
     * Instantiates a new error.
     * 
     * @param code the code
     * @param level the level
     */
    public Error(int code, int level) {
      this.code = code;
      this.level = level;
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
      return "Error #" + this.code;
    }
  }

  // Errors for API verification
  /** The parse error. */
  public static Error PARSE_ERROR = new Error(1, ERROR);
  
  /** The added package. */
  public static Error ADDED_PACKAGE = new Error(2, WARNING);
  
  /** The added class. */
  public static Error ADDED_CLASS = new Error(3, WARNING);
  
  /** The added method. */
  public static Error ADDED_METHOD = new Error(4, WARNING);
  
  /** The added field. */
  public static Error ADDED_FIELD = new Error(5, WARNING);
  
  /** The added interface. */
  public static Error ADDED_INTERFACE = new Error(6, WARNING);
  
  /** The removed package. */
  public static Error REMOVED_PACKAGE = new Error(7, WARNING);
  
  /** The removed class. */
  public static Error REMOVED_CLASS = new Error(8, WARNING);
  
  /** The removed method. */
  public static Error REMOVED_METHOD = new Error(9, WARNING);
  
  /** The removed field. */
  public static Error REMOVED_FIELD = new Error(10, WARNING);
  
  /** The removed interface. */
  public static Error REMOVED_INTERFACE = new Error(11, WARNING);
  
  /** The changed static. */
  public static Error CHANGED_STATIC = new Error(12, WARNING);
  
  /** The changed final. */
  public static Error CHANGED_FINAL = new Error(13, WARNING);
  
  /** The changed transient. */
  public static Error CHANGED_TRANSIENT = new Error(14, WARNING);
  
  /** The changed volatile. */
  public static Error CHANGED_VOLATILE = new Error(15, WARNING);
  
  /** The changed type. */
  public static Error CHANGED_TYPE = new Error(16, WARNING);
  
  /** The changed value. */
  public static Error CHANGED_VALUE = new Error(17, WARNING);
  
  /** The changed superclass. */
  public static Error CHANGED_SUPERCLASS = new Error(18, WARNING);
  
  /** The changed scope. */
  public static Error CHANGED_SCOPE = new Error(19, WARNING);
  
  /** The changed abstract. */
  public static Error CHANGED_ABSTRACT = new Error(20, WARNING);
  
  /** The changed throws. */
  public static Error CHANGED_THROWS = new Error(21, WARNING);
  
  /** The changed native. */
  public static Error CHANGED_NATIVE = new Error(22, HIDDEN);
  
  /** The changed class. */
  public static Error CHANGED_CLASS = new Error(23, WARNING);
  
  /** The changed deprecated. */
  public static Error CHANGED_DEPRECATED = new Error(24, WARNING);
  
  /** The changed synchronized. */
  public static Error CHANGED_SYNCHRONIZED = new Error(25, ERROR);

  // Errors in javadoc generation
  /** The Constant UNRESOLVED_LINK. */
  public static final Error UNRESOLVED_LINK = new Error(101, WARNING);
  
  /** The Constant BAD_INCLUDE_TAG. */
  public static final Error BAD_INCLUDE_TAG = new Error(102, WARNING);
  
  /** The Constant UNKNOWN_TAG. */
  public static final Error UNKNOWN_TAG = new Error(103, WARNING);
  
  /** The Constant UNKNOWN_PARAM_TAG_NAME. */
  public static final Error UNKNOWN_PARAM_TAG_NAME = new Error(104, WARNING);
  
  /** The Constant UNDOCUMENTED_PARAMETER. */
  public static final Error UNDOCUMENTED_PARAMETER = new Error(105, HIDDEN);
  
  /** The Constant BAD_ATTR_TAG. */
  public static final Error BAD_ATTR_TAG = new Error(106, ERROR);
  
  /** The Constant BAD_INHERITDOC. */
  public static final Error BAD_INHERITDOC = new Error(107, HIDDEN);
  
  /** The Constant HIDDEN_LINK. */
  public static final Error HIDDEN_LINK = new Error(108, WARNING);
  
  /** The Constant HIDDEN_CONSTRUCTOR. */
  public static final Error HIDDEN_CONSTRUCTOR = new Error(109, WARNING);
  
  /** The Constant UNAVAILABLE_SYMBOL. */
  public static final Error UNAVAILABLE_SYMBOL = new Error(110, ERROR);
  
  /** The Constant HIDDEN_SUPERCLASS. */
  public static final Error HIDDEN_SUPERCLASS = new Error(111, WARNING);
  
  /** The Constant DEPRECATED. */
  public static final Error DEPRECATED = new Error(112, HIDDEN);
  
  /** The Constant DEPRECATION_MISMATCH. */
  public static final Error DEPRECATION_MISMATCH = new Error(113, WARNING);
  
  /** The Constant MISSING_COMMENT. */
  public static final Error MISSING_COMMENT = new Error(114, WARNING);
  
  /** The Constant IO_ERROR. */
  public static final Error IO_ERROR = new Error(115, HIDDEN);
  
  /** The Constant NO_SINCE_DATA. */
  public static final Error NO_SINCE_DATA = new Error(116, HIDDEN);
  
  /** The Constant NO_FEDERATION_DATA. */
  public static final Error NO_FEDERATION_DATA = new Error(117, WARNING);
  
  /** The Constant BROKEN_SINCE_FILE. */
  public static final Error BROKEN_SINCE_FILE = new Error(118, ERROR);

  /** The Constant ERRORS. */
  public static final Error[] ERRORS =
      {UNRESOLVED_LINK, BAD_INCLUDE_TAG, UNKNOWN_TAG, UNKNOWN_PARAM_TAG_NAME,
          UNDOCUMENTED_PARAMETER, BAD_ATTR_TAG, BAD_INHERITDOC, HIDDEN_LINK, HIDDEN_CONSTRUCTOR,
          UNAVAILABLE_SYMBOL, HIDDEN_SUPERCLASS, DEPRECATED, DEPRECATION_MISMATCH, MISSING_COMMENT,
          IO_ERROR, NO_SINCE_DATA, NO_FEDERATION_DATA, PARSE_ERROR, ADDED_PACKAGE, ADDED_CLASS,
          ADDED_METHOD, ADDED_FIELD, ADDED_INTERFACE, REMOVED_PACKAGE, REMOVED_CLASS,
          REMOVED_METHOD, REMOVED_FIELD, REMOVED_INTERFACE, CHANGED_STATIC, CHANGED_FINAL,
          CHANGED_TRANSIENT, CHANGED_VOLATILE, CHANGED_TYPE, CHANGED_VALUE, CHANGED_SUPERCLASS,
          CHANGED_SCOPE, CHANGED_ABSTRACT, CHANGED_THROWS, CHANGED_NATIVE, CHANGED_CLASS,
          CHANGED_DEPRECATED, CHANGED_SYNCHRONIZED, BROKEN_SINCE_FILE};

  /**
     * Sets the error level.
     * 
     * @param code the code
     * @param level the level
     * @return true, if successful
     */
  public static boolean setErrorLevel(int code, int level) {
    for (Error e : ERRORS) {
      if (e.code == code) {
        e.level = level;
        return true;
      }
    }
    return false;
  }
}
