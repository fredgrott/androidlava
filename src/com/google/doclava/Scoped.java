/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

// TODO: Auto-generated Javadoc
/**
 * The Interface Scoped.
 */
public interface Scoped {
  
  /**
     * Checks if is public.
     * 
     * @return true, if is public
     */
  boolean isPublic();

  /**
     * Checks if is protected.
     * 
     * @return true, if is protected
     */
  boolean isProtected();

  /**
     * Checks if is package private.
     * 
     * @return true, if is package private
     */
  boolean isPackagePrivate();

  /**
     * Checks if is private.
     * 
     * @return true, if is private
     */
  boolean isPrivate();

  /**
     * Checks if is hidden.
     * 
     * @return true, if is hidden
     */
  boolean isHidden();
}
