/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;

// TODO: Auto-generated Javadoc
/**
 * The Class KeywordEntry.
 */
@SuppressWarnings("rawtypes")
class KeywordEntry implements Comparable {
  
  /**
     * Instantiates a new keyword entry.
     * 
     * @param label the label
     * @param href the href
     * @param comment the comment
     */
  KeywordEntry(String label, String href, String comment) {
    this.label = label;
    this.href = href;
    this.comment = comment;
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     */
  public void makeHDF(Data data, String base) {
    data.setValue(base + ".label", this.label);
    data.setValue(base + ".href", this.href);
    data.setValue(base + ".comment", this.comment);
  }

  /**
     * First char.
     * 
     * @return the char
     */
  public char firstChar() {
    return Character.toUpperCase(this.label.charAt(0));
  }

  /**
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(Object that) {
    return this.label.compareToIgnoreCase(((KeywordEntry) that).label);
  }

  /** The label. */
  private String label;
  
  /** The href. */
  private String href;
  
  /** The comment. */
  private String comment;
}
