/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;

import com.sun.javadoc.*;
import java.util.*;

// TODO: Auto-generated Javadoc
/**
 * The Class PackageInfo.
 */
public class PackageInfo extends DocInfo implements ContainerInfo {
  
  /** The Constant DEFAULT_PACKAGE. */
  public static final String DEFAULT_PACKAGE = "default package";

  /** The Constant comparator. */
  public static final Comparator<PackageInfo> comparator = new Comparator<PackageInfo>() {
    public int compare(PackageInfo a, PackageInfo b) {
      return a.name().compareTo(b.name());
    }
  };

  /**
     * Instantiates a new package info.
     * 
     * @param pkg the pkg
     * @param name the name
     * @param position the position
     */
  public PackageInfo(PackageDoc pkg, String name, SourcePositionInfo position) {
    super(pkg.getRawCommentText(), position);
    if (name.isEmpty()) {
      mName = DEFAULT_PACKAGE;
    } else {
      mName = name;
    }

    mPackage = pkg;
    initializeMaps();
  }

  /**
     * Instantiates a new package info.
     * 
     * @param name the name
     */
  public PackageInfo(String name) {
    super("", null);
    mName = name;
    initializeMaps();
  }

  /**
     * Instantiates a new package info.
     * 
     * @param name the name
     * @param position the position
     */
  public PackageInfo(String name, SourcePositionInfo position) {
    super("", position);

    if (name.isEmpty()) {
      mName = "default package";
    } else {
      mName = name;
    }
    initializeMaps();
  }

  /**
     * Initialize maps.
     */
  private void initializeMaps() {
      mInterfacesMap = new HashMap<String, ClassInfo>();
      mOrdinaryClassesMap = new HashMap<String, ClassInfo>();
      mEnumsMap = new HashMap<String, ClassInfo>();
      mExceptionsMap = new HashMap<String, ClassInfo>();
      mErrorsMap = new HashMap<String, ClassInfo>();
  }

  /**
   * @see com.google.doclava.DocInfo#htmlPage()
   */
  public String htmlPage() {
    String s = mName;
    s = s.replace('.', '/');
    s += "/package-summary.html";
    s = Doclava.javadocDir + s;
    return s;
  }

  /**
   * @see com.google.doclava.DocInfo#parent()
   */
  @Override
  public ContainerInfo parent() {
    return null;
  }

  /**
   * @see com.google.doclava.DocInfo#isHidden()
   */
  @Override
  public boolean isHidden() {
    return comment().isHidden();
  }

  /**
   * @see com.google.doclava.ContainerInfo#checkLevel()
   */
  public boolean checkLevel() {
    // TODO should return false if all classes are hidden but the package isn't.
    // We don't have this so I'm not doing it now.
    return !isHidden();
  }

  /**
     * Name.
     * 
     * @return the string
     */
  public String name() {
    return mName;
  }

  /**
   * @see com.google.doclava.ContainerInfo#qualifiedName()
   */
  public String qualifiedName() {
    return mName;
  }

  /**
     * Inline tags.
     * 
     * @return the tag info[]
     */
  public TagInfo[] inlineTags() {
    return comment().tags();
  }

  /**
     * First sentence tags.
     * 
     * @return the tag info[]
     */
  public TagInfo[] firstSentenceTags() {
    return comment().briefTags();
  }

  /**
     * Filter hidden.
     * 
     * @param classes the classes
     * @return the class info[]
     */
  public static ClassInfo[] filterHidden(ClassInfo[] classes) {
    ArrayList<ClassInfo> out = new ArrayList<ClassInfo>();

    for (ClassInfo cl : classes) {
      if (!cl.isHidden()) {
        out.add(cl);
      }
    }

    return out.toArray(new ClassInfo[0]);
  }

  /**
     * Make link.
     * 
     * @param data the data
     * @param base the base
     */
  public void makeLink(Data data, String base) {
    if (checkLevel()) {
      data.setValue(base + ".link", htmlPage());
    }
    data.setValue(base + ".name", name());
    data.setValue(base + ".since", getSince());
  }

  /**
     * Make class link list hdf.
     * 
     * @param data the data
     * @param base the base
     */
  public void makeClassLinkListHDF(Data data, String base) {
    makeLink(data, base);
    ClassInfo.makeLinkListHDF(data, base + ".interfaces", interfaces());
    ClassInfo.makeLinkListHDF(data, base + ".classes", ordinaryClasses());
    ClassInfo.makeLinkListHDF(data, base + ".enums", enums());
    ClassInfo.makeLinkListHDF(data, base + ".exceptions", exceptions());
    ClassInfo.makeLinkListHDF(data, base + ".errors", errors());
    data.setValue(base + ".since", getSince());
  }

  /**
     * Interfaces.
     * 
     * @return the class info[]
     */
  public ClassInfo[] interfaces() {
    if (mInterfaces == null) {
      mInterfaces =
          ClassInfo.sortByName(filterHidden(Converter.convertClasses(mPackage.interfaces())));
    }
    return mInterfaces;
  }

  /**
     * Ordinary classes.
     * 
     * @return the class info[]
     */
  public ClassInfo[] ordinaryClasses() {
    if (mOrdinaryClasses == null) {
      mOrdinaryClasses =
          ClassInfo.sortByName(filterHidden(Converter.convertClasses(mPackage.ordinaryClasses())));
    }
    return mOrdinaryClasses;
  }

  /**
     * Enums.
     * 
     * @return the class info[]
     */
  public ClassInfo[] enums() {
    if (mEnums == null) {
      mEnums = ClassInfo.sortByName(filterHidden(Converter.convertClasses(mPackage.enums())));
    }
    return mEnums;
  }

  /**
     * Exceptions.
     * 
     * @return the class info[]
     */
  public ClassInfo[] exceptions() {
    if (mExceptions == null) {
      mExceptions =
          ClassInfo.sortByName(filterHidden(Converter.convertClasses(mPackage.exceptions())));
    }
    return mExceptions;
  }

  /**
     * Errors.
     * 
     * @return the class info[]
     */
  public ClassInfo[] errors() {
    if (mErrors == null) {
      mErrors = ClassInfo.sortByName(filterHidden(Converter.convertClasses(mPackage.errors())));
    }
    return mErrors;
  }

  // in hashed containers, treat the name as the key
  /**
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return mName.hashCode();
  }

  /** The m name. */
  private String mName;
  
  /** The m package. */
  private PackageDoc mPackage;
  
  /** The m interfaces. */
  private ClassInfo[] mInterfaces;
  
  /** The m ordinary classes. */
  private ClassInfo[] mOrdinaryClasses;
  
  /** The m enums. */
  private ClassInfo[] mEnums;
  
  /** The m exceptions. */
  private ClassInfo[] mExceptions;
  
  /** The m errors. */
  private ClassInfo[] mErrors;

  /** The m interfaces map. */
  private HashMap<String, ClassInfo> mInterfacesMap;
  
  /** The m ordinary classes map. */
  private HashMap<String, ClassInfo> mOrdinaryClassesMap;
  
  /** The m enums map. */
  private HashMap<String, ClassInfo> mEnumsMap;
  
  /** The m exceptions map. */
  private HashMap<String, ClassInfo> mExceptionsMap;
  
  /** The m errors map. */
  private HashMap<String, ClassInfo> mErrorsMap;


  /**
     * Gets the class.
     * 
     * @param className the class name
     * @return the class
     */
  public ClassInfo getClass(String className) {
      ClassInfo cls = mInterfacesMap.get(className);

      if (cls != null) {
          return cls;
      }

      cls = mOrdinaryClassesMap.get(className);

      if (cls != null) {
          return cls;
      }

      cls = mEnumsMap.get(className);

      if (cls != null) {
          return cls;
      }

      cls = mEnumsMap.get(className);

      if (cls != null) {
          return cls;
      }

      return mErrorsMap.get(className);
  }

  /**
     * Adds the interface.
     * 
     * @param cls the cls
     */
  public void addInterface(ClassInfo cls) {
      mInterfacesMap.put(cls.name(), cls);
  }

  /**
     * Gets the interface.
     * 
     * @param interfaceName the interface name
     * @return the interface
     */
  public ClassInfo getInterface(String interfaceName) {
      return mInterfacesMap.get(interfaceName);
  }

  /**
     * Gets the ordinary class.
     * 
     * @param className the class name
     * @return the ordinary class
     */
  public ClassInfo getOrdinaryClass(String className) {
      return mOrdinaryClassesMap.get(className);
  }

  /**
     * Adds the ordinary class.
     * 
     * @param cls the cls
     */
  public void addOrdinaryClass(ClassInfo cls) {
      mOrdinaryClassesMap.put(cls.name(), cls);
  }

  /**
     * Gets the enum.
     * 
     * @param enumName the enum name
     * @return the enum
     */
  public ClassInfo getEnum(String enumName) {
      return mEnumsMap.get(enumName);
  }

  /**
     * Adds the enum.
     * 
     * @param cls the cls
     */
  public void addEnum(ClassInfo cls) {
      this.mEnumsMap.put(cls.name(), cls);
  }

  /**
     * Gets the exception.
     * 
     * @param exceptionName the exception name
     * @return the exception
     */
  public ClassInfo getException(String exceptionName) {
      return mExceptionsMap.get(exceptionName);
  }

  /**
     * Gets the error.
     * 
     * @param errorName the error name
     * @return the error
     */
  public ClassInfo getError(String errorName) {
      return mErrorsMap.get(errorName);
  }

  // TODO: Leftovers from ApiCheck that should be better merged.
  /** The m classes. */
  private HashMap<String, ClassInfo> mClasses = new HashMap<String, ClassInfo>();

  /**
     * Adds the class.
     * 
     * @param cl the cl
     */
  public void addClass(ClassInfo cl) {
    mClasses.put(cl.name(), cl);
  }

  /**
     * All classes.
     * 
     * @return the hash map
     */
  public HashMap<String, ClassInfo> allClasses() {
    return mClasses;
  }

  /**
     * Checks if is consistent.
     * 
     * @param pInfo the info
     * @return true, if is consistent
     */
  public boolean isConsistent(PackageInfo pInfo) {
    boolean consistent = true;
    for (ClassInfo cInfo : mClasses.values()) {
      if (pInfo.mClasses.containsKey(cInfo.name())) {
        if (!cInfo.isConsistent(pInfo.mClasses.get(cInfo.name()))) {
          consistent = false;
        }
      } else {
        Errors.error(Errors.REMOVED_CLASS, cInfo.position(), "Removed public class "
            + cInfo.qualifiedName());
        consistent = false;
      }
    }
    for (ClassInfo cInfo : pInfo.mClasses.values()) {
      if (!mClasses.containsKey(cInfo.name())) {
        Errors.error(Errors.ADDED_CLASS, cInfo.position(), "Added class " + cInfo.name()
            + " to package " + pInfo.name());
        consistent = false;
      }
    }
    return consistent;
  }
}
