/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;
import com.sun.javadoc.ClassDoc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

// TODO: Auto-generated Javadoc
/**
 * The Class ClassInfo.
 */
@SuppressWarnings("rawtypes")
public class ClassInfo extends DocInfo implements ContainerInfo, Comparable, Scoped, Resolvable {
  
  /** The Constant comparator. */
  public static final Comparator<ClassInfo> comparator = new Comparator<ClassInfo>() {
    public int compare(ClassInfo a, ClassInfo b) {
      return a.name().compareTo(b.name());
    }
  };

  /** The Constant qualifiedComparator. */
  public static final Comparator<ClassInfo> qualifiedComparator = new Comparator<ClassInfo>() {
    public int compare(ClassInfo a, ClassInfo b) {
      return a.qualifiedName().compareTo(b.qualifiedName());
    }
  };
  
  /**
     * Constructs a stub representation of a class.
     * 
     * @param qualifiedName the qualified name
     */
  public ClassInfo(String qualifiedName) {
    super("", SourcePositionInfo.UNKNOWN);
    
    mQualifiedName = qualifiedName;
    if (qualifiedName.lastIndexOf('.') != -1) {
      mName = qualifiedName.substring(qualifiedName.lastIndexOf('.') + 1);
    } else {
      mName = qualifiedName;
    }
  }

  /**
     * Instantiates a new class info.
     * 
     * @param cl the cl
     * @param rawCommentText the raw comment text
     * @param position the position
     * @param isPublic the is public
     * @param isProtected the is protected
     * @param isPackagePrivate the is package private
     * @param isPrivate the is private
     * @param isStatic the is static
     * @param isInterface the is interface
     * @param isAbstract the is abstract
     * @param isOrdinaryClass the is ordinary class
     * @param isException the is exception
     * @param isError the is error
     * @param isEnum the is enum
     * @param isAnnotation the is annotation
     * @param isFinal the is final
     * @param isIncluded the is included
     * @param name the name
     * @param qualifiedName the qualified name
     * @param qualifiedTypeName the qualified type name
     * @param isPrimitive the is primitive
     */
  public ClassInfo(ClassDoc cl, String rawCommentText, SourcePositionInfo position,
          boolean isPublic, boolean isProtected, boolean isPackagePrivate, boolean isPrivate,
          boolean isStatic, boolean isInterface, boolean isAbstract, boolean isOrdinaryClass,
          boolean isException, boolean isError, boolean isEnum, boolean isAnnotation, boolean isFinal,
          boolean isIncluded, String name, String qualifiedName, String qualifiedTypeName,
          boolean isPrimitive) {
      super(rawCommentText, position);

      initialize(rawCommentText, position,
              isPublic, isProtected, isPackagePrivate, isPrivate,
              isStatic, isInterface, isAbstract, isOrdinaryClass,
              isException, isError, isEnum, isAnnotation, isFinal,
              isIncluded, qualifiedTypeName, isPrimitive, null);

      mName = name;
      mQualifiedName = qualifiedName;
      mNameParts = name.split("\\.");
      mClass = cl;
  }

  /**
     * Initialize.
     * 
     * @param rawCommentText the raw comment text
     * @param position the position
     * @param isPublic the is public
     * @param isProtected the is protected
     * @param isPackagePrivate the is package private
     * @param isPrivate the is private
     * @param isStatic the is static
     * @param isInterface the is interface
     * @param isAbstract the is abstract
     * @param isOrdinaryClass the is ordinary class
     * @param isException the is exception
     * @param isError the is error
     * @param isEnum the is enum
     * @param isAnnotation the is annotation
     * @param isFinal the is final
     * @param isIncluded the is included
     * @param qualifiedTypeName the qualified type name
     * @param isPrimitive the is primitive
     * @param annotations the annotations
     */
  public void initialize(String rawCommentText, SourcePositionInfo position,
          boolean isPublic, boolean isProtected, boolean isPackagePrivate, boolean isPrivate,
          boolean isStatic, boolean isInterface, boolean isAbstract, boolean isOrdinaryClass,
          boolean isException, boolean isError, boolean isEnum, boolean isAnnotation, boolean isFinal,
          boolean isIncluded, String qualifiedTypeName, boolean isPrimitive, ArrayList<AnnotationInstanceInfo> annotations) {

    // calls
    setPosition(position);
    setRawCommentText(rawCommentText);
    mIsPublic = isPublic;
    mIsProtected = isProtected;
    mIsPackagePrivate = isPackagePrivate;
    mIsPrivate = isPrivate;
    mIsStatic = isStatic;
    mIsInterface = isInterface;
    mIsAbstract = isAbstract;
    mIsOrdinaryClass = isOrdinaryClass;
    mIsException = isException;
    mIsError = isError;
    mIsEnum = isEnum;
    mIsAnnotation = isAnnotation;
    mIsFinal = isFinal;
    mIsIncluded = isIncluded;
    mQualifiedTypeName = qualifiedTypeName;
    mIsPrimitive = isPrimitive;
    mAnnotations = annotations;
  }

  /**
     * Inits the.
     * 
     * @param typeInfo the type info
     * @param interfaces the interfaces
     * @param interfaceTypes the interface types
     * @param innerClasses the inner classes
     * @param constructors the constructors
     * @param methods the methods
     * @param annotationElements the annotation elements
     * @param fields the fields
     * @param enumConstants the enum constants
     * @param containingPackage the containing package
     * @param containingClass the containing class
     * @param superclass the superclass
     * @param superclassType the superclass type
     * @param annotations the annotations
     */
  public void init(TypeInfo typeInfo, ArrayList<ClassInfo> interfaces,
          ArrayList<TypeInfo> interfaceTypes, ArrayList<ClassInfo> innerClasses,
          ArrayList<MethodInfo> constructors, ArrayList<MethodInfo> methods,
          ArrayList<MethodInfo> annotationElements, ArrayList<FieldInfo> fields,
          ArrayList<FieldInfo> enumConstants, PackageInfo containingPackage,
          ClassInfo containingClass, ClassInfo superclass,
      TypeInfo superclassType, ArrayList<AnnotationInstanceInfo> annotations) {
    mTypeInfo = typeInfo;
    mRealInterfaces = new ArrayList<ClassInfo>(interfaces);
    mRealInterfaceTypes = interfaceTypes;
    mInnerClasses = innerClasses;
    mAllConstructors = constructors;
    mAllSelfMethods = methods;
    mAnnotationElements = annotationElements;
    mAllSelfFields = fields;
    mEnumConstants = enumConstants;
    mContainingPackage = containingPackage;
    mContainingClass = containingClass;
    mRealSuperclass = superclass;
    mRealSuperclassType = superclassType;
    mAnnotations = annotations;

    // after providing new methods and new superclass info,clear any cached
    // lists of self + superclass methods, ctors, etc.
    mSuperclassInit = false;
    mConstructors = null;
    mMethods = null;
    mSelfMethods = null;
    mFields = null;
    mSelfFields = null;
    mSelfAttributes = null;
    mDeprecatedKnown = false;

    Collections.sort(mEnumConstants, FieldInfo.comparator);
    Collections.sort(mInnerClasses, ClassInfo.comparator);
  }

  /**
     * Init2.
     */
  public void init2() {
    // calling this here forces the AttrTagInfo objects to be linked to the AttribtueInfo
    // objects
    selfAttributes();
  }

  /**
     * Init3.
     * 
     * @param types the types
     * @param realInnerClasses the real inner classes
     */
  public void init3(ArrayList<TypeInfo> types, ArrayList<ClassInfo> realInnerClasses) {
    mTypeParameters = types;
    mRealInnerClasses = realInnerClasses;
  }

  /**
     * Gets the real inner classes.
     * 
     * @return the real inner classes
     */
  public ArrayList<ClassInfo> getRealInnerClasses() {
    return mRealInnerClasses;
  }

  /**
     * Gets the type parameters.
     * 
     * @return the type parameters
     */
  public ArrayList<TypeInfo> getTypeParameters() {
    return mTypeParameters;
  }

  /**
   * @see com.google.doclava.ContainerInfo#checkLevel()
   */
  public boolean checkLevel() {
    int val = mCheckLevel;
    if (val >= 0) {
      return val != 0;
    } else {
      boolean v =
          Doclava.checkLevel(mIsPublic, mIsProtected, mIsPackagePrivate, mIsPrivate, isHidden());
      mCheckLevel = v ? 1 : 0;
      return v;
    }
  }

  /**
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  public int compareTo(Object that) {
    if (that instanceof ClassInfo) {
      return mQualifiedName.compareTo(((ClassInfo) that).mQualifiedName);
    } else {
      return this.hashCode() - that.hashCode();
    }
  }

  /**
   * @see com.google.doclava.DocInfo#parent()
   */
  @Override
  public ContainerInfo parent() {
    return this;
  }

  /**
   * @see com.google.doclava.Scoped#isPublic()
   */
  public boolean isPublic() {
    return mIsPublic;
  }

  /**
   * @see com.google.doclava.Scoped#isProtected()
   */
  public boolean isProtected() {
    return mIsProtected;
  }

  /**
   * @see com.google.doclava.Scoped#isPackagePrivate()
   */
  public boolean isPackagePrivate() {
    return mIsPackagePrivate;
  }

  /**
   * @see com.google.doclava.Scoped#isPrivate()
   */
  public boolean isPrivate() {
    return mIsPrivate;
  }

  /**
     * Checks if is static.
     * 
     * @return true, if is static
     */
  public boolean isStatic() {
    return mIsStatic;
  }

  /**
     * Checks if is interface.
     * 
     * @return true, if is interface
     */
  public boolean isInterface() {
    return mIsInterface;
  }

  /**
     * Checks if is abstract.
     * 
     * @return true, if is abstract
     */
  public boolean isAbstract() {
    return mIsAbstract;
  }

  /**
     * Containing package.
     * 
     * @return the package info
     */
  public PackageInfo containingPackage() {
    return mContainingPackage;
  }

  /**
     * Containing class.
     * 
     * @return the class info
     */
  public ClassInfo containingClass() {
    return mContainingClass;
  }

  /**
     * Checks if is ordinary class.
     * 
     * @return true, if is ordinary class
     */
  public boolean isOrdinaryClass() {
    return mIsOrdinaryClass;
  }

  /**
     * Checks if is exception.
     * 
     * @return true, if is exception
     */
  public boolean isException() {
    return mIsException;
  }

  /**
     * Checks if is error.
     * 
     * @return true, if is error
     */
  public boolean isError() {
    return mIsError;
  }

  /**
     * Checks if is enum.
     * 
     * @return true, if is enum
     */
  public boolean isEnum() {
    return mIsEnum;
  }

  /**
     * Checks if is annotation.
     * 
     * @return true, if is annotation
     */
  public boolean isAnnotation() {
    return mIsAnnotation;
  }

  /**
     * Checks if is final.
     * 
     * @return true, if is final
     */
  public boolean isFinal() {
    return mIsFinal;
  }

  /**
     * Checks if is included.
     * 
     * @return true, if is included
     */
  public boolean isIncluded() {
    return mIsIncluded;
  }

  /**
     * Type variables.
     * 
     * @return the hash set
     */
  public HashSet<String> typeVariables() {
    HashSet<String> result = TypeInfo.typeVariables(mTypeInfo.typeArguments());
    ClassInfo cl = containingClass();
    while (cl != null) {
      ArrayList<TypeInfo> types = cl.asTypeInfo().typeArguments();
      if (types != null) {
        TypeInfo.typeVariables(types, result);
      }
      cl = cl.containingClass();
    }
    return result;
  }

  /**
     * Gather hidden interfaces.
     * 
     * @param cl the cl
     * @param interfaces the interfaces
     */
  private static void gatherHiddenInterfaces(ClassInfo cl, HashSet<ClassInfo> interfaces) {
    for (ClassInfo iface : cl.mRealInterfaces) {
      if (iface.checkLevel()) {
        interfaces.add(iface);
      } else {
        gatherHiddenInterfaces(iface, interfaces);
      }
    }
  }

  /**
     * Interfaces.
     * 
     * @return the array list
     */
  public ArrayList<ClassInfo> interfaces() {
    if (mInterfaces == null) {
      if (checkLevel()) {
        HashSet<ClassInfo> interfaces = new HashSet<ClassInfo>();
        ClassInfo superclass = mRealSuperclass;
        while (superclass != null && !superclass.checkLevel()) {
          gatherHiddenInterfaces(superclass, interfaces);
          superclass = superclass.mRealSuperclass;
        }
        gatherHiddenInterfaces(this, interfaces);
        mInterfaces = new ArrayList<ClassInfo>(interfaces);
      } else {
        // put something here in case someone uses it
        mInterfaces = new ArrayList<ClassInfo>(mRealInterfaces);
      }
      Collections.sort(mInterfaces, ClassInfo.qualifiedComparator);
    }
    return mInterfaces;
  }

  /**
     * Real interfaces.
     * 
     * @return the array list
     */
  public ArrayList<ClassInfo> realInterfaces() {
    return mRealInterfaces;
  }

  /**
     * Real interface types.
     * 
     * @return the array list
     */
  ArrayList<TypeInfo> realInterfaceTypes() {
    return mRealInterfaceTypes;
  }

  /**
     * Adds the interface type.
     * 
     * @param type the type
     */
  public void addInterfaceType(TypeInfo type) {
      if (mRealInterfaceTypes == null) {
          mRealInterfaceTypes = new ArrayList<TypeInfo>();
      }

      mRealInterfaceTypes.add(type);
  }

  /**
     * Name.
     * 
     * @return the string
     */
  public String name() {
    return mName;
  }

  /**
     * Name parts.
     * 
     * @return the string[]
     */
  public String[] nameParts() {
    return mNameParts;
  }

  /**
     * Leaf name.
     * 
     * @return the string
     */
  public String leafName() {
    return mNameParts[mNameParts.length - 1];
  }

  /**
   * @see com.google.doclava.ContainerInfo#qualifiedName()
   */
  public String qualifiedName() {
    return mQualifiedName;
  }

  /**
     * Qualified type name.
     * 
     * @return the string
     */
  public String qualifiedTypeName() {
    return mQualifiedTypeName;
  }

  /**
     * Checks if is primitive.
     * 
     * @return true, if is primitive
     */
  public boolean isPrimitive() {
    return mIsPrimitive;
  }

  /**
     * All constructors.
     * 
     * @return the array list
     */
  public ArrayList<MethodInfo> allConstructors() {
    return mAllConstructors;
  }

  /**
     * Constructors.
     * 
     * @return the array list
     */
  public ArrayList<MethodInfo> constructors() {
    if (mConstructors == null) {
      if (mAllConstructors == null) {
        return new ArrayList<MethodInfo>();
      }

      mConstructors = new ArrayList<MethodInfo>();
      for (MethodInfo m : mAllConstructors) {
        if (!m.isHidden()) {
            mConstructors.add(m);
        }
      }

      Collections.sort(mConstructors, MethodInfo.comparator);
    }
    return mConstructors;
  }

  /**
     * Inner classes.
     * 
     * @return the array list
     */
  public ArrayList<ClassInfo> innerClasses() {
    return mInnerClasses;
  }

  /**
     * Inline tags.
     * 
     * @return the tag info[]
     */
  public TagInfo[] inlineTags() {
    return comment().tags();
  }

  /**
     * First sentence tags.
     * 
     * @return the tag info[]
     */
  public TagInfo[] firstSentenceTags() {
    return comment().briefTags();
  }

  /**
     * Sets the deprecated.
     * 
     * @param deprecated the new deprecated
     */
  public void setDeprecated(boolean deprecated) {
    mDeprecatedKnown = true;
    mIsDeprecated = deprecated;
  }

  /**
     * Checks if is deprecated.
     * 
     * @return true, if is deprecated
     */
  public boolean isDeprecated() {
    if (!mDeprecatedKnown) {
      boolean commentDeprecated = comment().isDeprecated();
      boolean annotationDeprecated = false;
      for (AnnotationInstanceInfo annotation : annotations()) {
        if (annotation.type().qualifiedName().equals("java.lang.Deprecated")) {
          annotationDeprecated = true;
          break;
        }
      }

      if (commentDeprecated != annotationDeprecated) {
        Errors.error(Errors.DEPRECATION_MISMATCH, position(), "Class " + qualifiedName()
            + ": @Deprecated annotation and @deprecated comment do not match");
      }

      mIsDeprecated = commentDeprecated | annotationDeprecated;
      mDeprecatedKnown = true;
    }
    return mIsDeprecated;
  }

  /**
     * Deprecated tags.
     * 
     * @return the tag info[]
     */
  public TagInfo[] deprecatedTags() {
    // Should we also do the interfaces?
    return comment().deprecatedTags();
  }

  /**
     * Methods.
     * 
     * @return the array list
     */
  public ArrayList<MethodInfo> methods() {
      if (mMethods == null) {
          TreeMap<String, MethodInfo> all = new TreeMap<String, MethodInfo>();

          ArrayList<ClassInfo> interfaces = interfaces();
          for (ClassInfo iface : interfaces) {
            if (iface != null) {
              for (MethodInfo method : iface.methods()) {
                all.put(method.getHashableName(), method);
              }
            }
          }

          ClassInfo superclass = superclass();
          if (superclass != null) {
            for (MethodInfo method : superclass.methods()) {
                all.put(method.getHashableName(), method);
            }
          }

          for (MethodInfo method : selfMethods()) {
              all.put(method.getHashableName(), method);
          }

          mMethods = new ArrayList<MethodInfo>(all.values());
          Collections.sort(mMethods, MethodInfo.comparator);
      }
    return mMethods;
  }

  /**
     * Annotation elements.
     * 
     * @return the array list
     */
  public ArrayList<MethodInfo> annotationElements() {
    return mAnnotationElements;
  }

  /**
     * Annotations.
     * 
     * @return the array list
     */
  public ArrayList<AnnotationInstanceInfo> annotations() {
    return mAnnotations;
  }

  /**
     * Adds the fields.
     * 
     * @param cl the cl
     * @param all the all
     */
  private static void addFields(ClassInfo cl, TreeMap<String, FieldInfo> all) {
    for (FieldInfo field : cl.fields()) {
        all.put(field.name(), field);
    }
  }

  /**
     * Fields.
     * 
     * @return the array list
     */
  public ArrayList<FieldInfo> fields() {
    if (mFields == null) {
      TreeMap<String, FieldInfo> all = new TreeMap<String, FieldInfo>();

      for (ClassInfo iface : interfaces()) {
        addFields(iface, all);
      }

      ClassInfo superclass = superclass();
      if (superclass != null) {
        addFields(superclass, all);
      }

      for (FieldInfo field : selfFields()) {
        if (!field.isHidden()) {
            all.put(field.name(), field);
        }
      }

      mFields = new ArrayList<FieldInfo>(all.values());
    }
    return mFields;
  }

  /**
     * Gather fields.
     * 
     * @param owner the owner
     * @param cl the cl
     * @param fields the fields
     */
  public void gatherFields(ClassInfo owner, ClassInfo cl, HashMap<String, FieldInfo> fields) {
    for (FieldInfo f : cl.selfFields()) {
      if (f.checkLevel()) {
        fields.put(f.name(), f.cloneForClass(owner));
      }
    }
  }

  /**
     * Self fields.
     * 
     * @return the array list
     */
  public ArrayList<FieldInfo> selfFields() {
    if (mSelfFields == null) {
        HashMap<String, FieldInfo> fields = new HashMap<String, FieldInfo>();
      // our hidden parents
      if (mRealSuperclass != null && !mRealSuperclass.checkLevel()) {
        gatherFields(this, mRealSuperclass, fields);
      }
      for (ClassInfo iface : mRealInterfaces) {
        if (!iface.checkLevel()) {
          gatherFields(this, iface, fields);
        }
      }

      for (FieldInfo f : mAllSelfFields) {
          if (!f.isHidden()) {
              fields.put(f.name(), f);
          }
      }

      mSelfFields = new ArrayList<FieldInfo>(fields.values());
      Collections.sort(mSelfFields, FieldInfo.comparator);
    }
    return mSelfFields;
  }

  /**
     * All self fields.
     * 
     * @return the array list
     */
  public ArrayList<FieldInfo> allSelfFields() {
    return mAllSelfFields;
  }

  /**
     * Gather methods.
     * 
     * @param owner the owner
     * @param cl the cl
     * @param methods the methods
     */
  private void gatherMethods(ClassInfo owner, ClassInfo cl, HashMap<String, MethodInfo> methods) {
    for (MethodInfo m : cl.selfMethods()) {
      if (m.checkLevel()) {
        methods.put(m.name() + m.signature(), m.cloneForClass(owner));
      }
    }
  }

  /**
     * Self methods.
     * 
     * @return the array list
     */
  public ArrayList<MethodInfo> selfMethods() {
    if (mSelfMethods == null) {
        HashMap<String, MethodInfo> methods = new HashMap<String, MethodInfo>();
      // our hidden parents
      if (mRealSuperclass != null && !mRealSuperclass.checkLevel()) {
        gatherMethods(this, mRealSuperclass, methods);
      }
      for (ClassInfo iface : mRealInterfaces) {
        if (!iface.checkLevel()) {
          gatherMethods(this, iface, methods);
        }
      }
      // mine
      if (mAllSelfMethods != null) {
        for (MethodInfo m : mAllSelfMethods) {
          if (m.checkLevel()) {
              methods.put(m.name() + m.signature(), m);
          }
        }
      }

      // sort it
      mSelfMethods = new ArrayList<MethodInfo>(methods.values());
      Collections.sort(mSelfMethods, MethodInfo.comparator);
    }
    return mSelfMethods;
  }

  /**
     * All self methods.
     * 
     * @return the array list
     */
  public ArrayList<MethodInfo> allSelfMethods() {
    return mAllSelfMethods;
  }

  /**
     * Adds the method.
     * 
     * @param method the method
     */
  public void addMethod(MethodInfo method) {
    mApiCheckMethods.put(method.getHashableName(), method);

    mAllSelfMethods.add(method);
    mSelfMethods = null; // flush this, hopefully it hasn't been used yet.
  }

  /**
     * Adds the annotation element.
     * 
     * @param method the method
     */
  public void addAnnotationElement(MethodInfo method) {
      mAnnotationElements.add(method);
  }

  /**
     * Sets the containing package.
     * 
     * @param pkg the new containing package
     */
  public void setContainingPackage(PackageInfo pkg) {
    mContainingPackage = pkg;

    if (mContainingPackage != null) {
        if (mIsEnum) {
            mContainingPackage.addEnum(this);
        } else if (mIsInterface) {
            mContainingPackage.addInterface(this);
        } else {
            mContainingPackage.addOrdinaryClass(this);
        }
    }
  }

  /**
     * Self attributes.
     * 
     * @return the array list
     */
  public ArrayList<AttributeInfo> selfAttributes() {
    if (mSelfAttributes == null) {
      TreeMap<FieldInfo, AttributeInfo> attrs = new TreeMap<FieldInfo, AttributeInfo>();

      // the ones in the class comment won't have any methods
      for (AttrTagInfo tag : comment().attrTags()) {
        FieldInfo field = tag.reference();
        if (field != null) {
          AttributeInfo attr = attrs.get(field);
          if (attr == null) {
            attr = new AttributeInfo(this, field);
            attrs.put(field, attr);
          }
          tag.setAttribute(attr);
        }
      }

      // in the methods
      for (MethodInfo m : selfMethods()) {
        for (AttrTagInfo tag : m.comment().attrTags()) {
          FieldInfo field = tag.reference();
          if (field != null) {
            AttributeInfo attr = attrs.get(field);
            if (attr == null) {
              attr = new AttributeInfo(this, field);
              attrs.put(field, attr);
            }
            tag.setAttribute(attr);
            attr.methods.add(m);
          }
        }
      }

      // constructors too
      for (MethodInfo m : constructors()) {
        for (AttrTagInfo tag : m.comment().attrTags()) {
          FieldInfo field = tag.reference();
          if (field != null) {
            AttributeInfo attr = attrs.get(field);
            if (attr == null) {
              attr = new AttributeInfo(this, field);
              attrs.put(field, attr);
            }
            tag.setAttribute(attr);
            attr.methods.add(m);
          }
        }
      }

      mSelfAttributes = new ArrayList<AttributeInfo>(attrs.values());
      Collections.sort(mSelfAttributes, AttributeInfo.comparator);
    }
    return mSelfAttributes;
  }

  /**
     * Enum constants.
     * 
     * @return the array list
     */
  public ArrayList<FieldInfo> enumConstants() {
    return mEnumConstants;
  }

  /**
     * Superclass.
     * 
     * @return the class info
     */
  public ClassInfo superclass() {
    if (!mSuperclassInit) {
      if (this.checkLevel()) {
        // rearrange our little inheritance hierarchy, because we need to hide classes that
        // don't pass checkLevel
        ClassInfo superclass = mRealSuperclass;
        while (superclass != null && !superclass.checkLevel()) {
          superclass = superclass.mRealSuperclass;
        }
        mSuperclass = superclass;
      } else {
        mSuperclass = mRealSuperclass;
      }
    }
    return mSuperclass;
  }

  /**
     * Real superclass.
     * 
     * @return the class info
     */
  public ClassInfo realSuperclass() {
    return mRealSuperclass;
  }

  /**
     * always the real superclass, not the collapsed one we get through
     * superclass(), also has the type parameter info if it's generic.
     * 
     * @return the type info
     */
  public TypeInfo superclassType() {
    return mRealSuperclassType;
  }

  /**
     * As type info.
     * 
     * @return the type info
     */
  public TypeInfo asTypeInfo() {
    return mTypeInfo;
  }

  /**
     * Interface types.
     * 
     * @return the array list
     */
  ArrayList<TypeInfo> interfaceTypes() {
      ArrayList<TypeInfo> types = new ArrayList<TypeInfo>();
      for (ClassInfo iface : interfaces()) {
          types.add(iface.asTypeInfo());
      }
      return types;
  }

  /**
   * @see com.google.doclava.DocInfo#htmlPage()
   */
  public String htmlPage() {
    String s = containingPackage().name();
    s = s.replace('.', '/');
    s += '/';
    s += name();
    s += ".html";
    s = Doclava.javadocDir + s;
    return s;
  }

  /**
     * Even indirectly.
     * 
     * @param cl the cl
     * @return true, if is derived from
     */
  public boolean isDerivedFrom(ClassInfo cl) {
    return isDerivedFrom(cl.qualifiedName());
  }

  /**
     * Even indirectly.
     * 
     * @param qualifiedName the qualified name
     * @return true, if is derived from
     */
  public boolean isDerivedFrom(String qualifiedName) {
    ClassInfo dad = this.superclass();
    if (dad != null) {
      if (dad.mQualifiedName.equals(qualifiedName)) {
        return true;
      } else {
        if (dad.isDerivedFrom(qualifiedName)) {
          return true;
        }
      }
    }
    for (ClassInfo iface : interfaces()) {
      if (iface.mQualifiedName.equals(qualifiedName)) {
        return true;
      } else {
        if (iface.isDerivedFrom(qualifiedName)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
     * Make keyword entries.
     * 
     * @param keywords the keywords
     */
  public void makeKeywordEntries(List<KeywordEntry> keywords) {
    if (!checkLevel()) {
      return;
    }

    String htmlPage = htmlPage();
    String qualifiedName = qualifiedName();

    keywords.add(new KeywordEntry(name(), htmlPage, "class in " + containingPackage().name()));

    ArrayList<FieldInfo> fields = selfFields();
    //ArrayList<FieldInfo> enumConstants = enumConstants();
    ArrayList<MethodInfo> ctors = constructors();
    ArrayList<MethodInfo> methods = selfMethods();

    // enum constants
    for (FieldInfo field : enumConstants()) {
      if (field.checkLevel()) {
        keywords.add(new KeywordEntry(field.name(), htmlPage + "#" + field.anchor(),
            "enum constant in " + qualifiedName));
      }
    }

    // constants
    for (FieldInfo field : fields) {
      if (field.isConstant() && field.checkLevel()) {
        keywords.add(new KeywordEntry(field.name(), htmlPage + "#" + field.anchor(), "constant in "
            + qualifiedName));
      }
    }

    // fields
    for (FieldInfo field : fields) {
      if (!field.isConstant() && field.checkLevel()) {
        keywords.add(new KeywordEntry(field.name(), htmlPage + "#" + field.anchor(), "field in "
            + qualifiedName));
      }
    }

    // public constructors
    for (MethodInfo m : ctors) {
      if (m.isPublic() && m.checkLevel()) {
        keywords.add(new KeywordEntry(m.prettySignature(), htmlPage + "#" + m.anchor(),
            "constructor in " + qualifiedName));
      }
    }

    // protected constructors
    if (Doclava.checkLevel(Doclava.SHOW_PROTECTED)) {
      for (MethodInfo m : ctors) {
        if (m.isProtected() && m.checkLevel()) {
          keywords.add(new KeywordEntry(m.prettySignature(),
              htmlPage + "#" + m.anchor(), "constructor in " + qualifiedName));
        }
      }
    }

    // package private constructors
    if (Doclava.checkLevel(Doclava.SHOW_PACKAGE)) {
      for (MethodInfo m : ctors) {
        if (m.isPackagePrivate() && m.checkLevel()) {
          keywords.add(new KeywordEntry(m.prettySignature(),
              htmlPage + "#" + m.anchor(), "constructor in " + qualifiedName));
        }
      }
    }

    // private constructors
    if (Doclava.checkLevel(Doclava.SHOW_PRIVATE)) {
      for (MethodInfo m : ctors) {
        if (m.isPrivate() && m.checkLevel()) {
          keywords.add(new KeywordEntry(m.name() + m.prettySignature(),
              htmlPage + "#" + m.anchor(), "constructor in " + qualifiedName));
        }
      }
    }

    // public methods
    for (MethodInfo m : methods) {
      if (m.isPublic() && m.checkLevel()) {
        keywords.add(new KeywordEntry(m.name() + m.prettySignature(), htmlPage + "#" + m.anchor(),
            "method in " + qualifiedName));
      }
    }

    // protected methods
    if (Doclava.checkLevel(Doclava.SHOW_PROTECTED)) {
      for (MethodInfo m : methods) {
        if (m.isProtected() && m.checkLevel()) {
          keywords.add(new KeywordEntry(m.name() + m.prettySignature(),
              htmlPage + "#" + m.anchor(), "method in " + qualifiedName));
        }
      }
    }

    // package private methods
    if (Doclava.checkLevel(Doclava.SHOW_PACKAGE)) {
      for (MethodInfo m : methods) {
        if (m.isPackagePrivate() && m.checkLevel()) {
          keywords.add(new KeywordEntry(m.name() + m.prettySignature(),
              htmlPage + "#" + m.anchor(), "method in " + qualifiedName));
        }
      }
    }

    // private methods
    if (Doclava.checkLevel(Doclava.SHOW_PRIVATE)) {
      for (MethodInfo m : methods) {
        if (m.isPrivate() && m.checkLevel()) {
          keywords.add(new KeywordEntry(m.name() + m.prettySignature(),
              htmlPage + "#" + m.anchor(), "method in " + qualifiedName));
        }
      }
    }
  }

  /**
     * Make link.
     * 
     * @param data the data
     * @param base the base
     */
  public void makeLink(Data data, String base) {
    data.setValue(base + ".label", this.name());
    if (!this.isPrimitive() && this.isIncluded() && this.checkLevel()) {
      data.setValue(base + ".link", this.htmlPage());
    }
  }

  /**
     * Make link list hdf.
     * 
     * @param data the data
     * @param base the base
     * @param classes the classes
     */
  public static void makeLinkListHDF(Data data, String base, ClassInfo[] classes) {
    final int N = classes.length;
    for (int i = 0; i < N; i++) {
      ClassInfo cl = classes[i];
      if (cl.checkLevel()) {
        cl.asTypeInfo().makeHDF(data, base + "." + i);
      }
    }
  }

  /**
     * Used in lists of this class (packages, nested classes, known subclasses).
     * 
     * @param data the data
     * @param base the base
     */
  public void makeShortDescrHDF(Data data, String base) {
    mTypeInfo.makeHDF(data, base + ".type");
    data.setValue(base + ".kind", this.kind());
    TagInfo.makeHDF(data, base + ".shortDescr", this.firstSentenceTags());
    TagInfo.makeHDF(data, base + ".deprecated", deprecatedTags());
    data.setValue(base + ".since", getSince());
    if (isDeprecated()) {
      data.setValue(base + ".deprecatedsince", getDeprecatedSince());
    }
    setFederatedReferences(data, base);
  }

  /**
     * Turns into the main class page.
     * 
     * @param data the data
     */
  public void makeHDF(Data data) {
    int i, j, n;
    String name = name();
    String qualified = qualifiedName();
    ArrayList<AttributeInfo> selfAttributes = selfAttributes();
    ArrayList<MethodInfo> methods = selfMethods();
    ArrayList<FieldInfo> fields = selfFields();
    ArrayList<FieldInfo> enumConstants = enumConstants();
    ArrayList<MethodInfo> ctors = constructors();
    ArrayList<ClassInfo> inners = innerClasses();

    // class name
    mTypeInfo.makeHDF(data, "class.type");
    mTypeInfo.makeQualifiedHDF(data, "class.qualifiedType");
    data.setValue("class.name", name);
    data.setValue("class.qualified", qualified);
    if (isProtected()) {
      data.setValue("class.scope", "protected");
    } else if (isPublic()) {
      data.setValue("class.scope", "public");
    }
    if (isStatic()) {
      data.setValue("class.static", "static");
    }
    if (isFinal()) {
      data.setValue("class.final", "final");
    }
    if (isAbstract() && !isInterface()) {
      data.setValue("class.abstract", "abstract");
    }

    // class info
    String kind = kind();
    if (kind != null) {
      data.setValue("class.kind", kind);
    }
    data.setValue("class.since", getSince());
    if (isDeprecated()) {
      data.setValue("class.deprecatedsince", getDeprecatedSince());
    }
    setFederatedReferences(data, "class");

    // the containing package -- note that this can be passed to type_link,
    // but it also contains the list of all of the packages
    containingPackage().makeClassLinkListHDF(data, "class.package");

    // inheritance hierarchy
    Vector<ClassInfo> superClasses = new Vector<ClassInfo>();
    superClasses.add(this);
    ClassInfo supr = superclass();
    while (supr != null) {
      superClasses.add(supr);
      supr = supr.superclass();
    }
    n = superClasses.size();
    for (i = 0; i < n; i++) {
      supr = superClasses.elementAt(n - i - 1);

      supr.asTypeInfo().makeQualifiedHDF(data, "class.inheritance." + i + ".class");
      supr.asTypeInfo().makeHDF(data, "class.inheritance." + i + ".short_class");
      j = 0;
      for (TypeInfo t : supr.interfaceTypes()) {
        t.makeHDF(data, "class.inheritance." + i + ".interfaces." + j);
        j++;
      }
    }

    // class description
    TagInfo.makeHDF(data, "class.descr", inlineTags());
    TagInfo.makeHDF(data, "class.seeAlso", comment().seeTags());
    TagInfo.makeHDF(data, "class.deprecated", deprecatedTags());

    // known subclasses
    TreeMap<String, ClassInfo> direct = new TreeMap<String, ClassInfo>();
    TreeMap<String, ClassInfo> indirect = new TreeMap<String, ClassInfo>();
    ClassInfo[] all = Converter.rootClasses();
    for (ClassInfo cl : all) {
      if (cl.superclass() != null && cl.superclass().equals(this)) {
        direct.put(cl.name(), cl);
      } else if (cl.isDerivedFrom(this)) {
        indirect.put(cl.name(), cl);
      }
    }
    // direct
    i = 0;
    for (ClassInfo cl : direct.values()) {
      if (cl.checkLevel()) {
        cl.makeShortDescrHDF(data, "class.subclasses.direct." + i);
      }
      i++;
    }
    // indirect
    i = 0;
    for (ClassInfo cl : indirect.values()) {
      if (cl.checkLevel()) {
        cl.makeShortDescrHDF(data, "class.subclasses.indirect." + i);
      }
      i++;
    }

    // hide special cases
    if ("java.lang.Object".equals(qualified) || "java.io.Serializable".equals(qualified)) {
      data.setValue("class.subclasses.hidden", "1");
    } else {
      data.setValue("class.subclasses.hidden", "0");
    }

    // nested classes
    i = 0;
    for (ClassInfo inner : inners) {
      if (inner.checkLevel()) {
        inner.makeShortDescrHDF(data, "class.inners." + i);
      }
      i++;
    }

    // enum constants
    i = 0;
    for (FieldInfo field : enumConstants) {
      field.makeHDF(data, "class.enumConstants." + i);
      i++;
    }

    // constants
    i = 0;
    for (FieldInfo field : fields) {
      if (field.isConstant()) {
        field.makeHDF(data, "class.constants." + i);
        i++;
      }
    }

    // fields
    i = 0;
    for (FieldInfo field : fields) {
      if (!field.isConstant()) {
        field.makeHDF(data, "class.fields." + i);
        i++;
      }
    }

    // public constructors
    i = 0;
    for (MethodInfo ctor : ctors) {
      if (ctor.isPublic()) {
        ctor.makeHDF(data, "class.ctors.public." + i);
        i++;
      }
    }

    // protected constructors
    if (Doclava.checkLevel(Doclava.SHOW_PROTECTED)) {
      i = 0;
      for (MethodInfo ctor : ctors) {
        if (ctor.isProtected()) {
          ctor.makeHDF(data, "class.ctors.protected." + i);
          i++;
        }
      }
    }

    // package private constructors
    if (Doclava.checkLevel(Doclava.SHOW_PACKAGE)) {
      i = 0;
      for (MethodInfo ctor : ctors) {
        if (ctor.isPackagePrivate()) {
          ctor.makeHDF(data, "class.ctors.package." + i);
          i++;
        }
      }
    }

    // private constructors
    if (Doclava.checkLevel(Doclava.SHOW_PRIVATE)) {
      i = 0;
      for (MethodInfo ctor : ctors) {
        if (ctor.isPrivate()) {
          ctor.makeHDF(data, "class.ctors.private." + i);
          i++;
        }
      }
    }

    // public methods
    i = 0;
    for (MethodInfo method : methods) {
      if (method.isPublic()) {
        method.makeHDF(data, "class.methods.public." + i);
        i++;
      }
    }

    // protected methods
    if (Doclava.checkLevel(Doclava.SHOW_PROTECTED)) {
      i = 0;
      for (MethodInfo method : methods) {
        if (method.isProtected()) {
          method.makeHDF(data, "class.methods.protected." + i);
          i++;
        }
      }
    }

    // package private methods
    if (Doclava.checkLevel(Doclava.SHOW_PACKAGE)) {
      i = 0;
      for (MethodInfo method : methods) {
        if (method.isPackagePrivate()) {
          method.makeHDF(data, "class.methods.package." + i);
          i++;
        }
      }
    }

    // private methods
    if (Doclava.checkLevel(Doclava.SHOW_PRIVATE)) {
      i = 0;
      for (MethodInfo method : methods) {
        if (method.isPrivate()) {
          method.makeHDF(data, "class.methods.private." + i);
          i++;
        }
      }
    }

    // xml attributes
    i = 0;
    for (AttributeInfo attr : selfAttributes) {
      if (attr.checkLevel()) {
        attr.makeHDF(data, "class.attrs." + i);
        i++;
      }
    }

    // inherited methods
    Set<ClassInfo> interfaces = new TreeSet<ClassInfo>();
    addInterfaces(interfaces(), interfaces);
    ClassInfo cl = superclass();
    i = 0;
    while (cl != null) {
      addInterfaces(cl.interfaces(), interfaces);
      makeInheritedHDF(data, i, cl);
      cl = cl.superclass();
      i++;
    }
    for (ClassInfo iface : interfaces) {
      makeInheritedHDF(data, i, iface);
      i++;
    }
  }

  /**
     * Adds the interfaces.
     * 
     * @param ifaces the ifaces
     * @param out the out
     */
  private static void addInterfaces(ArrayList<ClassInfo> ifaces, Set<ClassInfo> out) {
    for (ClassInfo cl : ifaces) {
      out.add(cl);
      addInterfaces(cl.interfaces(), out);
    }
  }

  /**
     * Make inherited hdf.
     * 
     * @param data the data
     * @param index the index
     * @param cl the cl
     */
  private static void makeInheritedHDF(Data data, int index, ClassInfo cl) {
    int i;

    String base = "class.inherited." + index;
    data.setValue(base + ".qualified", cl.qualifiedName());
    if (cl.checkLevel()) {
      data.setValue(base + ".link", cl.htmlPage());
    }
    String kind = cl.kind();
    if (kind != null) {
      data.setValue(base + ".kind", kind);
    }

    if (cl.mIsIncluded) {
      data.setValue(base + ".included", "true");
    } else {
      Doclava.federationTagger.tagAll(new ClassInfo[] {cl});
      if (!cl.getFederatedReferences().isEmpty()) {
        FederatedSite site = cl.getFederatedReferences().iterator().next();
        data.setValue(base + ".link", site.linkFor(cl.htmlPage()));
        data.setValue(base + ".federated", site.name());
      }
    }

    // xml attributes
    i = 0;
    for (AttributeInfo attr : cl.selfAttributes()) {
      attr.makeHDF(data, base + ".attrs." + i);
      i++;
    }

    // methods
    i = 0;
    for (MethodInfo method : cl.selfMethods()) {
      method.makeHDF(data, base + ".methods." + i);
      i++;
    }

    // fields
    i = 0;
    for (FieldInfo field : cl.selfFields()) {
      if (!field.isConstant()) {
        field.makeHDF(data, base + ".fields." + i);
        i++;
      }
    }

    // constants
    i = 0;
    for (FieldInfo field : cl.selfFields()) {
      if (field.isConstant()) {
        field.makeHDF(data, base + ".constants." + i);
        i++;
      }
    }
  }

  /**
   * @see com.google.doclava.DocInfo#isHidden()
   */
  @Override
  public boolean isHidden() {
    int val = mHidden;
    if (val >= 0) {
      return val != 0;
    } else {
      boolean v = isHiddenImpl();
      mHidden = v ? 1 : 0;
      return v;
    }
  }

  /**
     * Checks if is hidden impl.
     * 
     * @return true, if is hidden impl
     */
  public boolean isHiddenImpl() {
    ClassInfo cl = this;
    while (cl != null) {
      PackageInfo pkg = cl.containingPackage();
      if (pkg != null && pkg.isHidden()) {
        return true;
      }
      if (cl.comment().isHidden()) {
        return true;
      }
      cl = cl.containingClass();
    }
    return false;
  }

  /**
     * Match method.
     * 
     * @param methods the methods
     * @param name the name
     * @param params the params
     * @param dimensions the dimensions
     * @param varargs the varargs
     * @return the method info
     */
  private MethodInfo matchMethod(ArrayList<MethodInfo> methods, String name, String[] params,
      String[] dimensions, boolean varargs) {
    for (MethodInfo method : methods) {
      if (method.name().equals(name)) {
        if (params == null) {
          return method;
        } else {
          if (method.matchesParams(params, dimensions, varargs)) {
            return method;
          }
        }
      }
    }
    return null;
  }

  /**
     * Find method.
     * 
     * @param name the name
     * @param params the params
     * @param dimensions the dimensions
     * @param varargs the varargs
     * @return the method info
     */
  public MethodInfo findMethod(String name, String[] params, String[] dimensions, boolean varargs) {
    // first look on our class, and our superclasses

    // for methods
    MethodInfo rv;
    rv = matchMethod(methods(), name, params, dimensions, varargs);

    if (rv != null) {
      return rv;
    }

    // for constructors
    rv = matchMethod(constructors(), name, params, dimensions, varargs);
    if (rv != null) {
      return rv;
    }

    // then recursively look at our containing class
    ClassInfo containing = containingClass();
    if (containing != null) {
      return containing.findMethod(name, params, dimensions, varargs);
    }

    return null;
  }
  
  /**
     * Supports method.
     * 
     * @param method the method
     * @return true, if successful
     */
  public boolean supportsMethod(MethodInfo method) {
    for (MethodInfo m : methods()) {
      if (m.getHashableName().equals(method.getHashableName())) {
        return true;
      }
    }
    return false;
  }

  /**
     * Search inner classes.
     * 
     * @param nameParts the name parts
     * @param index the index
     * @return the class info
     */
  private ClassInfo searchInnerClasses(String[] nameParts, int index) {
    String part = nameParts[index];

    ArrayList<ClassInfo> inners = mInnerClasses;
    for (ClassInfo in : inners) {
      String[] innerParts = in.nameParts();
      if (part.equals(innerParts[innerParts.length - 1])) {
        if (index == nameParts.length - 1) {
          return in;
        } else {
          return in.searchInnerClasses(nameParts, index + 1);
        }
      }
    }
    return null;
  }

  /**
     * Extended find class.
     * 
     * @param className the class name
     * @return the class info
     */
  public ClassInfo extendedFindClass(String className) {
    // ClassDoc.findClass has this bug that we're working around here:
    // If you have a class PackageManager with an inner class PackageInfo
    // and you call it with "PackageInfo" it doesn't find it.
    return searchInnerClasses(className.split("\\."), 0);
  }

  /**
     * Find class.
     * 
     * @param className the class name
     * @return the class info
     */
  public ClassInfo findClass(String className) {
    return Converter.obtainClass(mClass.findClass(className));
  }

  /**
     * Find inner class.
     * 
     * @param className the class name
     * @return the class info
     */
  public ClassInfo findInnerClass(String className) {
    // ClassDoc.findClass won't find inner classes. To deal with that,
    // we try what they gave us first, but if that didn't work, then
    // we see if there are any periods in className, and start searching
    // from there.
    String[] nodes = className.split("\\.");
    ClassDoc cl = mClass;
    for (String n : nodes) {
      cl = cl.findClass(n);
      if (cl == null) {
        return null;
      }
    }
    return Converter.obtainClass(cl);
  }

  /**
     * Find field.
     * 
     * @param name the name
     * @return the field info
     */
  public FieldInfo findField(String name) {
    // first look on our class, and our superclasses
    for (FieldInfo f : fields()) {
      if (f.name().equals(name)) {
        return f;
      }
    }

    // then look at our enum constants (these are really fields, maybe
    // they should be mixed into fields(). not sure)
    for (FieldInfo f : enumConstants()) {
      if (f.name().equals(name)) {
        return f;
      }
    }

    // then recursively look at our containing class
    ClassInfo containing = containingClass();
    if (containing != null) {
      return containing.findField(name);
    }

    return null;
  }

  /**
     * Sort by name.
     * 
     * @param classes the classes
     * @return the class info[]
     */
  public static ClassInfo[] sortByName(ClassInfo[] classes) {
    int i;
    Sorter[] sorted = new Sorter[classes.length];
    for (i = 0; i < sorted.length; i++) {
      ClassInfo cl = classes[i];
      sorted[i] = new Sorter(cl.name(), cl);
    }

    Arrays.sort(sorted);

    ClassInfo[] rv = new ClassInfo[classes.length];
    for (i = 0; i < rv.length; i++) {
      rv[i] = (ClassInfo) sorted[i].data;
    }

    return rv;
  }

  /**
     * Equals.
     * 
     * @param that the that
     * @return true, if successful
     */
  public boolean equals(ClassInfo that) {
    if (that != null) {
      return this.qualifiedName().equals(that.qualifiedName());
    } else {
      return false;
    }
  }

  /**
     * Sets the non written constructors.
     * 
     * @param nonWritten the new non written constructors
     */
  public void setNonWrittenConstructors(ArrayList<MethodInfo> nonWritten) {
    mNonWrittenConstructors = nonWritten;
  }

  /**
     * Gets the non written constructors.
     * 
     * @return the non written constructors
     */
  public ArrayList<MethodInfo> getNonWrittenConstructors() {
    return mNonWrittenConstructors;
  }

  /**
     * Kind.
     * 
     * @return the string
     */
  public String kind() {
    if (isOrdinaryClass()) {
      return "class";
    } else if (isInterface()) {
      return "interface";
    } else if (isEnum()) {
      return "enum";
    } else if (isError()) {
      return "class";
    } else if (isException()) {
      return "class";
    } else if (isAnnotation()) {
      return "@interface";
    }
    return null;
  }
  
  /**
     * Scope.
     * 
     * @return the string
     */
  public String scope() {
    if (isPublic()) {
      return "public";
    } else if (isProtected()) {
      return "protected";
    } else if (isPackagePrivate()) {
      return "";
    } else if (isPrivate()) {
      return "private";
    } else {
      throw new RuntimeException("invalid scope for object " + this);
    }
  }

  /**
     * Sets the hidden methods.
     * 
     * @param mInfo the new hidden methods
     */
  public void setHiddenMethods(ArrayList<MethodInfo> mInfo) {
    mHiddenMethods = mInfo;
  }

  /**
     * Gets the hidden methods.
     * 
     * @return the hidden methods
     */
  public ArrayList<MethodInfo> getHiddenMethods() {
    return mHiddenMethods;
  }

  /**
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return this.qualifiedName();
  }

  /**
     * Sets the reason included.
     * 
     * @param reason the new reason included
     */
  public void setReasonIncluded(String reason) {
    mReasonIncluded = reason;
  }

  /**
     * Gets the reason included.
     * 
     * @return the reason included
     */
  public String getReasonIncluded() {
    return mReasonIncluded;
  }

  /** The m class. */
  private ClassDoc mClass;

  // ctor
  /** The m is public. */
  private boolean mIsPublic;
  
  /** The m is protected. */
  private boolean mIsProtected;
  
  /** The m is package private. */
  private boolean mIsPackagePrivate;
  
  /** The m is private. */
  private boolean mIsPrivate;
  
  /** The m is static. */
  private boolean mIsStatic;
  
  /** The m is interface. */
  private boolean mIsInterface;
  
  /** The m is abstract. */
  private boolean mIsAbstract;
  
  /** The m is ordinary class. */
  private boolean mIsOrdinaryClass;
  
  /** The m is exception. */
  private boolean mIsException;
  
  /** The m is error. */
  private boolean mIsError;
  
  /** The m is enum. */
  private boolean mIsEnum;
  
  /** The m is annotation. */
  private boolean mIsAnnotation;
  
  /** The m is final. */
  private boolean mIsFinal;
  
  /** The m is included. */
  private boolean mIsIncluded;
  
  /** The m name. */
  private String mName;
  
  /** The m qualified name. */
  private String mQualifiedName;
  
  /** The m qualified type name. */
  private String mQualifiedTypeName;
  
  /** The m is primitive. */
  private boolean mIsPrimitive;
  
  /** The m type info. */
  private TypeInfo mTypeInfo;
  
  /** The m name parts. */
  private String[] mNameParts;

  // init
  /** The m real interfaces. */
  private ArrayList<ClassInfo> mRealInterfaces = new ArrayList<ClassInfo>();
  
  /** The m interfaces. */
  private ArrayList<ClassInfo> mInterfaces;
  
  /** The m real interface types. */
  private ArrayList<TypeInfo> mRealInterfaceTypes;
  
  /** The m inner classes. */
  private ArrayList<ClassInfo> mInnerClasses;
  
  /** The m all constructors. */
  private ArrayList<MethodInfo> mAllConstructors = new ArrayList<MethodInfo>();
  
  /** The m all self methods. */
  private ArrayList<MethodInfo> mAllSelfMethods = new ArrayList<MethodInfo>();
  
  /** The m annotation elements. */
  private ArrayList<MethodInfo> mAnnotationElements = new ArrayList<MethodInfo>(); // if this class is an annotation
  
  /** The m all self fields. */
  private ArrayList<FieldInfo> mAllSelfFields = new ArrayList<FieldInfo>();
  
  /** The m enum constants. */
  private ArrayList<FieldInfo> mEnumConstants = new ArrayList<FieldInfo>();
  
  /** The m containing package. */
  private PackageInfo mContainingPackage;
  
  /** The m containing class. */
  private ClassInfo mContainingClass;
  
  /** The m real superclass. */
  private ClassInfo mRealSuperclass;
  
  /** The m real superclass type. */
  private TypeInfo mRealSuperclassType;
  
  /** The m superclass. */
  private ClassInfo mSuperclass;
  
  /** The m annotations. */
  private ArrayList<AnnotationInstanceInfo> mAnnotations;
  
  /** The m superclass init. */
  private boolean mSuperclassInit;
  
  /** The m deprecated known. */
  private boolean mDeprecatedKnown;

  // lazy
  /** The m constructors. */
  private ArrayList<MethodInfo> mConstructors;
  
  /** The m real inner classes. */
  private ArrayList<ClassInfo> mRealInnerClasses;
  
  /** The m self methods. */
  private ArrayList<MethodInfo> mSelfMethods;
  
  /** The m self fields. */
  private ArrayList<FieldInfo> mSelfFields;
  
  /** The m self attributes. */
  private ArrayList<AttributeInfo> mSelfAttributes;
  
  /** The m methods. */
  private ArrayList<MethodInfo> mMethods;
  
  /** The m fields. */
  private ArrayList<FieldInfo> mFields;
  
  /** The m type parameters. */
  private ArrayList<TypeInfo> mTypeParameters;
  
  /** The m hidden methods. */
  private ArrayList<MethodInfo> mHiddenMethods;
  
  /** The m hidden. */
  private int mHidden = -1;
  
  /** The m check level. */
  private int mCheckLevel = -1;
  
  /** The m reason included. */
  private String mReasonIncluded;
  
  /** The m non written constructors. */
  private ArrayList<MethodInfo> mNonWrittenConstructors;
  
  /** The m is deprecated. */
  private boolean mIsDeprecated;
  
  // TODO: Temporary members from apicheck migration.
  /** The m api check constructors. */
  private HashMap<String, MethodInfo> mApiCheckConstructors = new HashMap<String, MethodInfo>();
  
  /** The m api check methods. */
  private HashMap<String, MethodInfo> mApiCheckMethods = new HashMap<String, MethodInfo>();
  
  /** The m api check fields. */
  private HashMap<String, FieldInfo> mApiCheckFields = new HashMap<String, FieldInfo>();
  
  /** The m api check enum constants. */
  private HashMap<String, FieldInfo> mApiCheckEnumConstants = new HashMap<String, FieldInfo>();

  // Resolutions
  /** The m resolutions. */
  private ArrayList<Resolution> mResolutions;
  
  /**
     * Returns true if {@code cl} implements the interface {@code iface} either
     * by either being that interface, implementing that interface or extending
     * a type that implements the interface.
     * 
     * @param cl the cl
     * @param iface the iface
     * @return true, if successful
     */
  private boolean implementsInterface(ClassInfo cl, String iface) {
    if (cl.qualifiedName().equals(iface)) {
      return true;
    }
    for (ClassInfo clImplements : cl.interfaces()) {
      if (implementsInterface(clImplements, iface)) {
        return true;
      }
    }
    if (cl.mSuperclass != null && implementsInterface(cl.mSuperclass, iface)) {
      return true;
    }
    return false;
  }

  /**
     * Adds the interface.
     * 
     * @param iface the iface
     */
  public void addInterface(ClassInfo iface) {
    mRealInterfaces.add(iface);
  }

  /**
     * Adds the constructor.
     * 
     * @param ctor the ctor
     */
  public void addConstructor(MethodInfo ctor) {
    mApiCheckConstructors.put(ctor.getHashableName(), ctor);

    mAllConstructors.add(ctor);
    mConstructors = null; // flush this, hopefully it hasn't been used yet.
  }

  /**
     * Adds the field.
     * 
     * @param field the field
     */
  public void addField(FieldInfo field) {
    mApiCheckFields.put(field.name(), field);

    mAllSelfFields.add(field);

    mSelfFields = null; // flush this, hopefully it hasn't been used yet.
  }

  /**
     * Adds the enum constant.
     * 
     * @param field the field
     */
  public void addEnumConstant(FieldInfo field) {
    mApiCheckEnumConstants.put(field.name(), field);

    mEnumConstants.add(field);
  }

  /**
     * Sets the super class.
     * 
     * @param superclass the new super class
     */
  public void setSuperClass(ClassInfo superclass) {
    mRealSuperclass = superclass;
    mSuperclass = superclass;
  }

  /**
     * All constructors map.
     * 
     * @return the map
     */
  public Map<String, MethodInfo> allConstructorsMap() {
    return mApiCheckConstructors;
  }

  /**
     * All fields.
     * 
     * @return the map
     */
  public Map<String, FieldInfo> allFields() {
    return mApiCheckFields;
  }

  /**
     * Returns all methods defined directly in this class. For a list of all
     * methods supported by this class, see {@link #methods()}.
     * 
     * @return the map
     */
  
  public Map<String, MethodInfo> allMethods() {
    return mApiCheckMethods;
  }

  /**
     * Returns the class hierarchy for this class, starting with this class.
     * 
     * @return the iterable
     */
  public Iterable<ClassInfo> hierarchy() {
    List<ClassInfo> result = new ArrayList<ClassInfo>(4);
    for (ClassInfo c = this; c != null; c = c.mSuperclass) {
      result.add(c);
    }
    return result;
  }
  
  /**
     * Superclass name.
     * 
     * @return the string
     */
  public String superclassName() {
    if (mSuperclass == null) {
      if (mQualifiedName.equals("java.lang.Object")) {
        return null;
      }
      throw new UnsupportedOperationException("Superclass not set for " + qualifiedName());
    }
    return mSuperclass.mQualifiedName;
  }
  
  /**
     * Sets the annotations.
     * 
     * @param annotations the new annotations
     */
  public void setAnnotations(ArrayList<AnnotationInstanceInfo> annotations) {
    mAnnotations = annotations;
  }
  
  /**
     * Checks if is consistent.
     * 
     * @param cl the cl
     * @return true, if is consistent
     */
  public boolean isConsistent(ClassInfo cl) {
    boolean consistent = true;

    if (isInterface() != cl.isInterface()) {
      Errors.error(Errors.CHANGED_CLASS, cl.position(), "Class " + cl.qualifiedName()
          + " changed class/interface declaration");
      consistent = false;
    }
    for (ClassInfo iface : mRealInterfaces) {
      if (!implementsInterface(cl, iface.mQualifiedName)) {
        Errors.error(Errors.REMOVED_INTERFACE, cl.position(), "Class " + qualifiedName()
            + " no longer implements " + iface);
      }
    }
    for (ClassInfo iface : cl.mRealInterfaces) {
      if (!implementsInterface(this, iface.mQualifiedName)) {
        Errors.error(Errors.ADDED_INTERFACE, cl.position(), "Added interface " + iface
            + " to class " + qualifiedName());
        consistent = false;
      }
    }

    for (MethodInfo mInfo : mApiCheckMethods.values()) {
      if (cl.mApiCheckMethods.containsKey(mInfo.getHashableName())) {
        if (!mInfo.isConsistent(cl.mApiCheckMethods.get(mInfo.getHashableName()))) {
          consistent = false;
        }
      } else {
        /*
         * This class formerly provided this method directly, and now does not. Check our ancestry
         * to see if there's an inherited version that still fulfills the API requirement.
         */
        MethodInfo mi = ClassInfo.overriddenMethod(mInfo, cl);
        if (mi == null) {
          mi = ClassInfo.interfaceMethod(mInfo, cl);
        }
        if (mi == null) {
          Errors.error(Errors.REMOVED_METHOD, mInfo.position(), "Removed public method "
              + mInfo.qualifiedName());
          consistent = false;
        }
      }
    }
    for (MethodInfo mInfo : cl.mApiCheckMethods.values()) {
      if (!mApiCheckMethods.containsKey(mInfo.getHashableName())) {
        /*
         * Similarly to the above, do not fail if this "new" method is really an override of an
         * existing superclass method.
         */
        MethodInfo mi = ClassInfo.overriddenMethod(mInfo, this);
        if (mi == null) {
          Errors.error(Errors.ADDED_METHOD, mInfo.position(), "Added public method "
              + mInfo.qualifiedName());
          consistent = false;
        }
      }
    }

    for (MethodInfo mInfo : mApiCheckConstructors.values()) {
      if (cl.mApiCheckConstructors.containsKey(mInfo.getHashableName())) {
        if (!mInfo.isConsistent(cl.mApiCheckConstructors.get(mInfo.getHashableName()))) {
          consistent = false;
        }
      } else {
        Errors.error(Errors.REMOVED_METHOD, mInfo.position(), "Removed public constructor "
            + mInfo.prettySignature());
        consistent = false;
      }
    }
    for (MethodInfo mInfo : cl.mApiCheckConstructors.values()) {
      if (!mApiCheckConstructors.containsKey(mInfo.getHashableName())) {
        Errors.error(Errors.ADDED_METHOD, mInfo.position(), "Added public constructor "
            + mInfo.prettySignature());
        consistent = false;
      }
    }

    for (FieldInfo mInfo : mApiCheckFields.values()) {
      if (cl.mApiCheckFields.containsKey(mInfo.name())) {
        if (!mInfo.isConsistent(cl.mApiCheckFields.get(mInfo.name()))) {
          consistent = false;
        }
      } else {
        Errors.error(Errors.REMOVED_FIELD, mInfo.position(), "Removed field "
            + mInfo.qualifiedName());
        consistent = false;
      }
    }
    for (FieldInfo mInfo : cl.mApiCheckFields.values()) {
      if (!mApiCheckFields.containsKey(mInfo.name())) {
        Errors.error(Errors.ADDED_FIELD, mInfo.position(), "Added public field "
            + mInfo.qualifiedName());
        consistent = false;
      }
    }

    for (FieldInfo info : mApiCheckEnumConstants.values()) {
      if (cl.mApiCheckEnumConstants.containsKey(info.name())) {
        if (!info.isConsistent(cl.mApiCheckEnumConstants.get(info.name()))) {
          consistent = false;
        }
      } else {
        Errors.error(Errors.REMOVED_FIELD, info.position(), "Removed enum constant "
            + info.qualifiedName());
        consistent = false;
      }
    }
    for (FieldInfo info : cl.mApiCheckEnumConstants.values()) {
      if (!mApiCheckEnumConstants.containsKey(info.name())) {
        Errors.error(Errors.ADDED_FIELD, info.position(), "Added enum constant "
            + info.qualifiedName());
        consistent = false;
      }
    }

    if (mIsAbstract != cl.mIsAbstract) {
      consistent = false;
      Errors.error(Errors.CHANGED_ABSTRACT, cl.position(), "Class " + cl.qualifiedName()
          + " changed abstract qualifier");
    }

    if (mIsFinal != cl.mIsFinal) {
      consistent = false;
      Errors.error(Errors.CHANGED_FINAL, cl.position(), "Class " + cl.qualifiedName()
          + " changed final qualifier");
    }

    if (mIsStatic != cl.mIsStatic) {
      consistent = false;
      Errors.error(Errors.CHANGED_STATIC, cl.position(), "Class " + cl.qualifiedName()
          + " changed static qualifier");
    }

    if (!scope().equals(cl.scope())) {
      consistent = false;
      Errors.error(Errors.CHANGED_SCOPE, cl.position(), "Class " + cl.qualifiedName()
          + " scope changed from " + scope() + " to " + cl.scope());
    }

    if (!isDeprecated() == cl.isDeprecated()) {
      consistent = false;
      Errors.error(Errors.CHANGED_DEPRECATED, cl.position(), "Class " + cl.qualifiedName()
          + " has changed deprecation state");
    }

    if (superclassName() != null) {
      if (cl.superclassName() == null || !superclassName().equals(cl.superclassName())) {
        consistent = false;
        Errors.error(Errors.CHANGED_SUPERCLASS, cl.position(), "Class " + qualifiedName()
            + " superclass changed from " + superclassName() + " to " + cl.superclassName());
      }
    } else if (cl.superclassName() != null) {
      consistent = false;
      Errors.error(Errors.CHANGED_SUPERCLASS, cl.position(), "Class " + qualifiedName()
          + " superclass changed from " + "null to " + cl.superclassName());
    }

    return consistent;
  }
  
  // Find a superclass implementation of the given method.
  /**
     * Overridden method.
     * 
     * @param candidate the candidate
     * @param newClassObj the new class obj
     * @return the method info
     */
  public static MethodInfo overriddenMethod(MethodInfo candidate, ClassInfo newClassObj) {
    if (newClassObj == null) {
      return null;
    }
    for (MethodInfo mi : newClassObj.mApiCheckMethods.values()) {
      if (mi.matches(candidate)) {
        // found it
        return mi;
      }
    }

    // not found here. recursively search ancestors
    return ClassInfo.overriddenMethod(candidate, newClassObj.mSuperclass);
  }

  // Find a superinterface declaration of the given method.
  /**
     * Interface method.
     * 
     * @param candidate the candidate
     * @param newClassObj the new class obj
     * @return the method info
     */
  public static MethodInfo interfaceMethod(MethodInfo candidate, ClassInfo newClassObj) {
    if (newClassObj == null) {
      return null;
    }
    for (ClassInfo interfaceInfo : newClassObj.interfaces()) {
      for (MethodInfo mi : interfaceInfo.mApiCheckMethods.values()) {
        if (mi.matches(candidate)) {
          return mi;
        }
      }
    }
    return ClassInfo.interfaceMethod(candidate, newClassObj.mSuperclass);
  }
  
  /**
     * Checks for constructor.
     * 
     * @param constructor the constructor
     * @return true, if successful
     */
  public boolean hasConstructor(MethodInfo constructor) {
    String name = constructor.getHashableName();
    for (MethodInfo ctor : mApiCheckConstructors.values()) {
      if (name.equals(ctor.getHashableName())) {
        return true;
      }
    }
    return false;
  }

  /**
     * Sets the type info.
     * 
     * @param typeInfo the new type info
     */
  public void setTypeInfo(TypeInfo typeInfo) {
    mTypeInfo = typeInfo;
  }

  /**
     * Type.
     * 
     * @return the type info
     */
  public TypeInfo type() {
      return mTypeInfo;
  }

  /**
     * Adds the inner class.
     * 
     * @param innerClass the inner class
     */
  public void addInnerClass(ClassInfo innerClass) {
      if (mInnerClasses == null) {
          mInnerClasses = new ArrayList<ClassInfo>();
      }

      mInnerClasses.add(innerClass);
  }

  /**
     * Sets the containing class.
     * 
     * @param containingClass the new containing class
     */
  public void setContainingClass(ClassInfo containingClass) {
      mContainingClass = containingClass;
  }

  /**
     * Sets the superclass type.
     * 
     * @param superclassType the new superclass type
     */
  public void setSuperclassType(TypeInfo superclassType) {
      mRealSuperclassType = superclassType;
  }

  /**
   * @see com.google.doclava.Resolvable#printResolutions()
   */
  public void printResolutions() {
      if (mResolutions == null || mResolutions.isEmpty()) {
          return;
      }

      System.out.println("Resolutions for Class " + mName + ":");

      for (Resolution r : mResolutions) {
          System.out.println(r);
      }
  }

  /**
   * @see com.google.doclava.Resolvable#addResolution(com.google.doclava.Resolution)
   */
  public void addResolution(Resolution resolution) {
      if (mResolutions == null) {
          mResolutions = new ArrayList<Resolution>();
      }

      mResolutions.add(resolution);
  }

  /**
   * @see com.google.doclava.Resolvable#resolveResolutions()
   */
  public boolean resolveResolutions() {
      ArrayList<Resolution> resolutions = mResolutions;
      mResolutions = new ArrayList<Resolution>();

      boolean allResolved = true;
      for (Resolution resolution : resolutions) {
          StringBuilder qualifiedClassName = new StringBuilder();
          InfoBuilder.resolveQualifiedName(resolution.getValue(), qualifiedClassName,
                  resolution.getInfoBuilder());

          // if we still couldn't resolve it, save it for the next pass
          if ("".equals(qualifiedClassName.toString())) {
              mResolutions.add(resolution);
              allResolved = false;
          } else if ("superclassQualifiedName".equals(resolution.getVariable())) {
              setSuperClass(InfoBuilder.Caches.obtainClass(qualifiedClassName.toString()));
          } else if ("interfaceQualifiedName".equals(resolution.getVariable())) {
              addInterface(InfoBuilder.Caches.obtainClass(qualifiedClassName.toString()));
          }
      }

      return allResolved;
  }
}
