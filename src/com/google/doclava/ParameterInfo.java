/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;

import java.util.HashSet;

// TODO: Auto-generated Javadoc
/**
 * The Class ParameterInfo.
 */
public class ParameterInfo {
  
  /**
     * Instantiates a new parameter info.
     * 
     * @param name the name
     * @param typeName the type name
     * @param type the type
     * @param isVarArg the is var arg
     * @param position the position
     */
  public ParameterInfo(String name, String typeName, TypeInfo type, boolean isVarArg,
      SourcePositionInfo position) {
    mName = name;
    mTypeName = typeName;
    mType = type;
    mIsVarArg = isVarArg;
    mPosition = position;
  }

  /**
     * Type.
     * 
     * @return the type info
     */
  TypeInfo type() {
    return mType;
  }

  /**
     * Name.
     * 
     * @return the string
     */
  String name() {
    return mName;
  }

  /**
     * Type name.
     * 
     * @return the string
     */
  String typeName() {
    return mTypeName;
  }

  /**
     * Position.
     * 
     * @return the source position info
     */
  SourcePositionInfo position() {
    return mPosition;
  }
  
  /**
     * Checks if is var arg.
     * 
     * @return true, if is var arg
     */
  boolean isVarArg() {
    return mIsVarArg;
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param isLastVararg the is last vararg
     * @param typeVariables the type variables
     */
  public void makeHDF(Data data, String base, boolean isLastVararg, HashSet<String> typeVariables) {
    data.setValue(base + ".name", this.name());
    type().makeHDF(data, base + ".type", isLastVararg, typeVariables);
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param params the params
     * @param isVararg the is vararg
     * @param typeVariables the type variables
     */
  public static void makeHDF(Data data, String base, ParameterInfo[] params, boolean isVararg,
      HashSet<String> typeVariables) {
    for (int i = 0; i < params.length; i++) {
      params[i].makeHDF(data, base + "." + i, isVararg && (i == params.length - 1), typeVariables);
    }
  }
  
  /**
     * Returns true if this parameter's dimension information agrees with the
     * represented callee's dimension information.
     * 
     * @param dimension the dimension
     * @param varargs the varargs
     * @return true, if successful
     */
  public boolean matchesDimension(String dimension, boolean varargs) {
    if (varargs) {
      dimension += "[]";
    }
    return mType.dimension().equals(dimension);
  }

  /** The m name. */
  String mName;
  
  /** The m type name. */
  String mTypeName;
  
  /** The m type. */
  TypeInfo mType;
  
  /** The m is var arg. */
  boolean mIsVarArg;
  
  /** The m position. */
  SourcePositionInfo mPosition;
}
