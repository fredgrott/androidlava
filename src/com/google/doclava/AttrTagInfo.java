/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

// TODO: Auto-generated Javadoc
/**
 * The Class AttrTagInfo.
 */
public class AttrTagInfo extends TagInfo {
  
  /** The Constant REF_COMMAND. */
  private static final String REF_COMMAND = "ref";
  
  /** The Constant NAME_COMMAND. */
  private static final String NAME_COMMAND = "name";
  
  /** The Constant DESCRIPTION_COMMAND. */
  private static final String DESCRIPTION_COMMAND = "description";
  
  /** The Constant TEXT. */
  private static final Pattern TEXT = Pattern.compile("(\\S+)\\s*(.*)", Pattern.DOTALL);
  
  /** The Constant NAME_TEXT. */
  private static final Pattern NAME_TEXT = Pattern.compile("(\\S+)(.*)", Pattern.DOTALL);

  /** The m base. */
  private ContainerInfo mBase;
  
  /** The m command. */
  private String mCommand;

  // if mCommand == "ref"
  /** The m ref field. */
  private FieldInfo mRefField;
  
  /** The m attr info. */
  private AttributeInfo mAttrInfo;

  // if mCommand == "name"
  /** The m attr name. */
  private String mAttrName;

  // if mCommand == "description"
  /** The m descr comment. */
  private Comment mDescrComment;

  /**
     * Instantiates a new attr tag info.
     * 
     * @param name the name
     * @param kind the kind
     * @param text the text
     * @param base the base
     * @param position the position
     */
  AttrTagInfo(String name, String kind, String text, ContainerInfo base, SourcePositionInfo position) {
    super(name, kind, text, position);
    mBase = base;

    parse(text, base, position);
  }

  /**
     * Parses the.
     * 
     * @param text the text
     * @param base the base
     * @param position the position
     */
  void parse(String text, ContainerInfo base, SourcePositionInfo position) {
    Matcher m;

    m = TEXT.matcher(text);
    if (!m.matches()) {
      Errors.error(Errors.BAD_ATTR_TAG, position, "Bad @attr tag: " + text);
      return;
    }

    String command = m.group(1);
    String more = m.group(2);

    if (REF_COMMAND.equals(command)) {
      String ref = more.trim();
      LinkReference linkRef = LinkReference.parse(ref, mBase, position, false);
      if (!linkRef.good) {
        Errors.error(Errors.BAD_ATTR_TAG, position, "Unresolved @attr ref: " + ref);
        return;
      }
      if (!(linkRef.memberInfo instanceof FieldInfo)) {
        Errors.error(Errors.BAD_ATTR_TAG, position, "@attr must be a field: " + ref);
        return;
      }
      mCommand = command;
      mRefField = (FieldInfo) linkRef.memberInfo;
    } else if (NAME_COMMAND.equals(command)) {
      m = NAME_TEXT.matcher(more);
      if (!m.matches() || m.group(2).trim().length() != 0) {
        Errors.error(Errors.BAD_ATTR_TAG, position, "Bad @attr name tag: " + more);
        return;
      }
      mCommand = command;
      mAttrName = m.group(1);
    } else if (DESCRIPTION_COMMAND.equals(command)) {
      mCommand = command;
      mDescrComment = new Comment(more, base, position);
    } else {
      Errors.error(Errors.BAD_ATTR_TAG, position, "Bad @attr command: " + command);
    }
  }

  /**
     * Reference.
     * 
     * @return the field info
     */
  public FieldInfo reference() {
    return REF_COMMAND.equals(mCommand) ? mRefField : null;
  }

  /**
   * @see com.google.doclava.TagInfo#name()
   */
  @Override
  public String name() {
    return NAME_COMMAND.equals(mCommand) ? mAttrName : null;
  }

  /**
     * Description.
     * 
     * @return the comment
     */
  public Comment description() {
    return DESCRIPTION_COMMAND.equals(mCommand) ? mDescrComment : null;
  }

  /**
   * @see com.google.doclava.TagInfo#makeHDF(com.google.clearsilver.jsilver.data.Data, java.lang.String)
   */
  @Override
  public void makeHDF(Data data, String base) {
    super.makeHDF(data, base);
  }

  /**
     * Sets the attribute.
     * 
     * @param info the new attribute
     */
  public void setAttribute(AttributeInfo info) {
    mAttrInfo = info;
  }

  /**
     * Make reference hdf.
     * 
     * @param data the data
     * @param base the base
     * @param tags the tags
     */
  @SuppressWarnings("unused")
public static void makeReferenceHDF(Data data, String base, AttrTagInfo[] tags) {
    int i = 0;
    for (AttrTagInfo t : tags) {
      if (REF_COMMAND.equals(t.mCommand)) {
        if (t.mAttrInfo == null) {
          String msg = "ERROR: unlinked attr: " + t.mRefField.name();
          if (false) {
            System.out.println(msg);
          } else {
            throw new RuntimeException(msg);
          }
        } else {
          data.setValue(base + "." + i + ".name", t.mAttrInfo.name());
          data.setValue(base + "." + i + ".href", t.mAttrInfo.htmlPage());
          i++;
        }
      }
    }
  }

}
