/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

// TODO: Auto-generated Javadoc
/**
 * The Class ThrowsTagInfo.
 */
public class ThrowsTagInfo extends ParsedTagInfo {
  
  /** The Constant PATTERN. */
  static final Pattern PATTERN = Pattern.compile("(\\S+)\\s+(.*)", Pattern.DOTALL);
  
  /** The m exception. */
  private ClassInfo mException;

  /**
     * Instantiates a new throws tag info.
     * 
     * @param name the name
     * @param kind the kind
     * @param text the text
     * @param base the base
     * @param sp the sp
     */
  public ThrowsTagInfo(String name, String kind, String text, ContainerInfo base,
      SourcePositionInfo sp) {
    super(name, kind, text, base, sp);

    Matcher m = PATTERN.matcher(text);
    if (m.matches()) {
      setCommentText(m.group(2));
      String className = m.group(1);
      if (base instanceof ClassInfo) {
        mException = ((ClassInfo) base).findClass(className);
      }
      if (mException == null) {
        mException = Converter.obtainClass(className);
      }
    }
  }

  /**
     * Instantiates a new throws tag info.
     * 
     * @param name the name
     * @param kind the kind
     * @param text the text
     * @param exception the exception
     * @param exceptionComment the exception comment
     * @param base the base
     * @param sp the sp
     */
  public ThrowsTagInfo(String name, String kind, String text, ClassInfo exception,
      String exceptionComment, ContainerInfo base, SourcePositionInfo sp) {
    super(name, kind, text, base, sp);
    mException = exception;
    setCommentText(exceptionComment);
  }

  /**
     * Exception.
     * 
     * @return the class info
     */
  public ClassInfo exception() {
    return mException;
  }

  /**
     * Exception type.
     * 
     * @return the type info
     */
  public TypeInfo exceptionType() {
    if (mException != null) {
      return mException.asTypeInfo();
    } else {
      return null;
    }
  }

  /**
     * Make hdf.
     * 
     * @param data the data
     * @param base the base
     * @param tags the tags
     */
  public static void makeHDF(Data data, String base, ThrowsTagInfo[] tags) {
    for (int i = 0; i < tags.length; i++) {
      TagInfo.makeHDF(data, base + '.' + i + ".comment", tags[i].commentTags());
      if (tags[i].exceptionType() != null) {
        tags[i].exceptionType().makeHDF(data, base + "." + i + ".type");
      }
    }
  }


}
