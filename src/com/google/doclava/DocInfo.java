/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

import com.google.clearsilver.jsilver.data.Data;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * The Class DocInfo.
 */
@SuppressWarnings("unused")
public abstract class DocInfo {
  
  /**
     * Instantiates a new doc info.
     * 
     * @param rawCommentText the raw comment text
     * @param sp the sp
     */
  public DocInfo(String rawCommentText, SourcePositionInfo sp) {
    mRawCommentText = rawCommentText;
    mPosition = sp;
  }

  /**
     * The relative path to a web page representing this item.
     * 
     * @return the string
     */
  public abstract String htmlPage();
  
  /**
     * Checks if is hidden.
     * 
     * @return true, if is hidden
     */
  public boolean isHidden() {
    return comment().isHidden();
  }

  /**
     * Checks if is doc only.
     * 
     * @return true, if is doc only
     */
  public boolean isDocOnly() {
    return comment().isDocOnly();
  }

  /**
     * Gets the raw comment text.
     * 
     * @return the raw comment text
     */
  public String getRawCommentText() {
    return mRawCommentText;
  }

  /**
     * Sets the raw comment text.
     * 
     * @param rawCommentText the new raw comment text
     */
  public void setRawCommentText(String rawCommentText) {
      mRawCommentText = rawCommentText;

      // so that if we've created one prior to changing, we recreate it
      if (mComment != null) {
          mComment = new Comment(mRawCommentText, parent(), mPosition);
      }

  }

  /**
     * Comment.
     * 
     * @return the comment
     */
  public Comment comment() {
    if (mComment == null) {
      mComment = new Comment(mRawCommentText, parent(), mPosition);
    }
    return mComment;
  }

  /**
     * Position.
     * 
     * @return the source position info
     */
  public SourcePositionInfo position() {
    return mPosition;
  }

  /**
     * Sets the position.
     * 
     * @param position the new position
     */
  public void setPosition(SourcePositionInfo position) {
      mPosition = position;

      // so that if we've created one prior to changing, we recreate it
      if (mComment != null) {
          mComment = new Comment(mRawCommentText, parent(), mPosition);
      }
  }

  /**
     * Parent.
     * 
     * @return the container info
     */
  public abstract ContainerInfo parent();

  /**
     * Sets the since.
     * 
     * @param since the new since
     */
  public void setSince(String since) {
    mSince = since;
  }

  /**
     * Gets the since.
     * 
     * @return the since
     */
  public String getSince() {
    return mSince;
  }
  
  /**
     * Sets the deprecated since.
     * 
     * @param since the new deprecated since
     */
  public void setDeprecatedSince(String since) {
    mDeprecatedSince = since;
  }

  /**
     * Gets the deprecated since.
     * 
     * @return the deprecated since
     */
  public String getDeprecatedSince() {
    return mDeprecatedSince;
  }

  /**
     * Adds the federated reference.
     * 
     * @param source the source
     */
  public final void addFederatedReference(FederatedSite source) {
    mFederatedReferences.add(source);
  }
  
  /**
     * Gets the federated references.
     * 
     * @return the federated references
     */
  public final Set<FederatedSite> getFederatedReferences() {
    return mFederatedReferences;
  }
  
  /**
     * Sets the federated references.
     * 
     * @param data the data
     * @param base the base
     */
  public final void setFederatedReferences(Data data, String base) {
    int pos = 0;
    for (FederatedSite source : getFederatedReferences()) {
      data.setValue(base + ".federated." + pos + ".url", source.linkFor(htmlPage()));
      data.setValue(base + ".federated." + pos + ".name", source.name());
      pos++;
    }
  }

  /** The m raw comment text. */
  private String mRawCommentText;
  
  /** The m comment. */
  Comment mComment;
  
  /** The m position. */
  SourcePositionInfo mPosition;
  
  /** The m since. */
  private String mSince;
  
  /** The m deprecated since. */
  private String mDeprecatedSince;
  
  /** The m federated references. */
  private Set<FederatedSite> mFederatedReferences = new LinkedHashSet<FederatedSite>();
}
