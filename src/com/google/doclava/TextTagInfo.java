/*
 * Copyright (C) 2010 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.doclava;

// TODO: Auto-generated Javadoc
/**
 * The Class TextTagInfo.
 */
public class TextTagInfo extends TagInfo {
  
  /**
     * Instantiates a new text tag info.
     * 
     * @param n the n
     * @param k the k
     * @param t the t
     * @param p the p
     */
  TextTagInfo(String n, String k, String t, SourcePositionInfo p) {
    super(n, k, Doclava.escape(t), p);
  }
}
