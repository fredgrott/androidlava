AndroidLava
---

Small fork of DocLava as the GoogleCode version doesnot work as far as the 
embedd javadocs in website feature but the AOSP copy does so this just a small 
modification.

# Project License

[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

# Project Usage

# Project Build Directions

## local.properties variables are:

JDK tools jar location tool.jar 
graphviz dot executable location graphviz.exec
google analtyics id   google.analtyics.id



# Project Website




# Project Credits

Original credits go to the Google Engineers who created DocLava.

[DocLava at Google Code](http://code.google.com/p/doclava/)

[DocLava the AOSP copy I used](http://code.google.com/p/android-source-browsing/source/browse/?repo=platform--external--doclava#git)

## Modification Credits

Fred Grott

[Fred Grott's website](http://fredgrott.bitbucket.org)

[Fred Grott's blog](http://grottworkshop.blogspot.com)

